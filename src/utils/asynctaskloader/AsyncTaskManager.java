package utils.asynctaskloader;

import java.io.File;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Build;

public final class AsyncTaskManager implements IProgressTracker, OnCancelListener {
    
    private final OnTaskCompleteListener mTaskCompleteListener;
    private final ProgressDialog mProgressDialog;
    private NetTask mAsyncTask;

    public AsyncTaskManager(Context context, OnTaskCompleteListener taskCompleteListener) {
		// Save reference to complete listener (activity)
		mTaskCompleteListener = taskCompleteListener;
		// Setup progress dialog
		mProgressDialog = new ProgressDialog(context);
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(true);
		mProgressDialog.setOnCancelListener(this);
		//Set cashe dir
		enableHttpResponseCache(new File(context.getCacheDir(), "http"));
		disableConnectionReuseIfNecessary();
    }
    
    private void enableHttpResponseCache(File httpCacheDir) {
        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            Class.forName("android.net.http.HttpResponseCache")
                .getMethod("install", File.class, long.class)
                .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
        }
    }
    
    private void disableConnectionReuseIfNecessary() {
    	// HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < 0*8) {
            System.setProperty("http.keepAlive", "false");
        }
    }
    
    public void setupTask(NetTask asyncTask) {
		// Keep task
		mAsyncTask = asyncTask;
		// Wire task to tracker (this)
		mAsyncTask.setProgressTracker(this);
		// Start task
		mAsyncTask.execute();
    }

    @Override
    public void onProgress(String message) {
		// Show dialog if it wasn't shown yet or was removed on configuration (rotation) change
		if (!mProgressDialog.isShowing()) {
		    mProgressDialog.show();
		    //mProgressDialog.setContentView(R.layout.emptydialog);
		}
		// Show current message in progress dialog
		mProgressDialog.setMessage(message);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
		// Cancel task
		mAsyncTask.cancel(true);
		// Notify activity about completion
		mTaskCompleteListener.onTaskComplete(mAsyncTask);
		// Reset task
		mAsyncTask = null;
    }
    
    @Override
    public void onComplete() {
		// Close progress dialog
		mProgressDialog.dismiss();
		// Notify activity about completion
		if (mAsyncTask!=null)
			mTaskCompleteListener.onTaskComplete(mAsyncTask);
		// Reset task
		mAsyncTask = null;
    }

    public Object retainTask() {
		// Detach task from tracker (this) before retain
		if (mAsyncTask != null) {
		    mAsyncTask.setProgressTracker(null);
		}
		// Retain task
		return mAsyncTask;
    }

    public void handleRetainedTask(Object instance) {
		// Restore retained task and attach it to tracker (this)
		if (instance instanceof Task) {
		    mAsyncTask = (NetTask) instance;
		    mAsyncTask.setProgressTracker(this);
		}
    }

    public boolean isWorking() {
		// Track current status
		return mAsyncTask != null;
    }
}