package utils.fragmenttaskloader;

public interface ITaskLoaderListener {
	void onLoadFinished(Object data);
	void onCancelLoad();
}
