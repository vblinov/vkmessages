package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;

import com.androidquery.AQuery;

import java.util.ArrayList;

import widget.ScrollingTextView;

public class ActPhotos extends Activity {
	private static final String TAG = "ActPhotos";
	private ScrollingTextView title;
    private AQuery webView;
	private int ind = 0;
    private GestureDetector gestureDetector;
	ArrayList<ClsAttPhoto> photos;
    private static final int SWIPE_MIN_DISTANCE = 140;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private WebView.OnTouchListener gestureListener;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos);
        
        photos = AndroidApplication.getPhotos();
        if (photos==null || photos.size()==0) 
        	finish();
        title = (ScrollingTextView)findViewById(R.id.pfl_title);
        title.setText(getString(R.string.dlg_phototitle).replace("{1}", ""+(ind+1)).replace("{n}", ""+photos.size()));

        AQuery aq =new AQuery(this);
        gestureDetector = new GestureDetector(new MyGestureDetector());
        gestureListener = new WebView.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };
        webView =aq.id(R.id.web);
        webView.getWebView().setOnTouchListener(gestureListener);
        ClsAttPhoto photo = photos.get(0);
        webView.webImage(photo.getSrc_big());
    }

    class MyGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if(e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if (photos.size() > 1) {
                        ind++;
                        if (ind == photos.size()) ind = 0;
                        ClsAttPhoto photo = photos.get(ind);
                        webView.webImage(photo.getSrc_big());
                        title.setText(getString(R.string.dlg_phototitle).replace("{1}", "" + (ind + 1)).replace("{n}", "" + photos.size()));
                    }
                    else {
                        finish();
                    }

                }  else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    if (photos.size() > 1) {
                        ind--;
                        if (ind < 0) {
                            finish();
                        }
                        else {
                            ClsAttPhoto photo = photos.get(ind);
                            webView.webImage(photo.getSrc_big());
                            title.setText(getString(R.string.dlg_phototitle).replace("{1}", "" + (ind + 1)).replace("{n}", "" + photos.size()));
                        }

                    }
                    else {
                        finish();
                    }
                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }
    }
	public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActPhotos.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
	public void onBack(View v) {
		finish();
	}
}