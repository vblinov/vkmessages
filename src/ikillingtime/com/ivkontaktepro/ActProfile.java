package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import org.json.JSONObject;

import utils.imageloader.AsyncImageView;
import utils.imageloader.MaskImageProcessor;
import utils.serviceloader.RESTService;
import widget.ScrollingTextView;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ActProfile extends Activity {
	private static final String TAG = "ActProfile";
	public static String name="",phone="",url="";
	public static int uid=0;
	private ResultReceiver mReceiver;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        
        final ScrollingTextView title = (ScrollingTextView)findViewById(R.id.pfl_title);
        title.setText(name);
        final Button callBtn = (Button)findViewById(R.id.callBtn);
        callBtn.setText(getString(R.string.cnt_call).replace("{phone}", (""+phone)));
        if (phone==null || phone.equals(""))
        	callBtn.setVisibility(View.GONE);
        final AsyncImageView img =(AsyncImageView)findViewById(R.id.user_thumb);
        if (url.equals("")) {
        	img.setImageResource(R.drawable.contact_nophoto);
        }
        else {
        	img.setUrl(url);
        	img.setImageProcessor(new MaskImageProcessor(7));
        }
        final Button inviteBtn = (Button)findViewById(R.id.inviteBtn);
        final Button sendBtn = (Button)findViewById(R.id.sendBtn);
        final TextView notregister = (TextView)findViewById(R.id.notregister);
        final TextView del = (TextView)findViewById(R.id.del);
        if (uid<0) {
        	inviteBtn.setVisibility(View.VISIBLE);
        	sendBtn.setVisibility(View.GONE);
        	notregister.setVisibility(View.VISIBLE);
        	del.setVisibility(View.GONE);
        }
        else {
        	inviteBtn.setVisibility(View.GONE);
        	sendBtn.setVisibility(View.VISIBLE);
        	notregister.setVisibility(View.GONE);
        	del.setVisibility(View.VISIBLE);
        }
        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
       };
    }
	public void onRESTResult(int code, String result, int mode) {
		Log.d(TAG, "onRESTResult:"+result);
		if (code == 200 && result != null) {
        	
			if (result.contains("error_msg")) {
				try {
					JSONObject o = new JSONObject(result);
					o = o.optJSONObject("error");
					Toast.makeText(this, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
				}
				catch (Exception e) {}
				return;
			}
		}

	}
	public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActProfile.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }
	public void onBack(View v) {
		finish();
	}
	public void onDel(View v) {
		String code="var profiles = API.friends.delete({uid: "+uid+"});" +
        		"return {profiles: profiles};";
        restRequest(code,0);
		finish();
	}
	private String restRequest(String code,int mode) {
    	Log.d(TAG, code);
    	String metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));       
        Intent intent = new Intent(this, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig));
        Bundle params = new Bundle();
        params.putString("code", code);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        startService(intent);
        return sig;
	}
	public void onCall(View v) {
		callIntent(phone);
	}
	public void onSend(View v) {
		startActivity(ActDialog.createIntent(this));
	}
	public void onInvite(View v) {
		smsIntent(phone,getString(R.string.pfl_join),name);
	}
	private void smsIntent(String phone,String smsText,String personName) {

    	Intent smsIntent = new Intent(Intent.ACTION_VIEW);
		smsIntent.putExtra("address", phone);
		smsIntent.setType("vnd.android-dir/mms-sms");
		smsIntent.putExtra("sms_body", personName+", "+smsText);
		
	    try {
	        startActivity(smsIntent);
	    } 
	    catch (ActivityNotFoundException e) {
	    	Toast.makeText(this,e.toString(), Toast.LENGTH_LONG).show();
	    }
    }
	private void callIntent(String personPhone) {

    	Intent callIntent = new Intent(Intent.ACTION_CALL);
    	
		callIntent.setData(Uri.parse("tel:" + personPhone));
        try {
        	startActivity(callIntent);
        } 
        catch (Exception e) 
        {
        	Toast.makeText(this, e.toString(), Toast.LENGTH_LONG).show();
        }
	}
}


