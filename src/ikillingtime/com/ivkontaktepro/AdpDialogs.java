package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.util.ArrayList;

import utils.imageloader.AsyncImageView;
import utils.imageloader.ChainImageProcessor;
import utils.imageloader.ImageProcessor;
import utils.imageloader.MaskImageProcessor;
import utils.imageloader.ScaleImageProcessor;
import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class AdpDialogs extends BaseAdapter {
    
	private static final String TAG = "AdpDialogs";
    private Activity activity;
    private ArrayList<ClsDialog> data;
    private LayoutInflater inflater=null;
    private ImageProcessor mImageProcessor,mImageProcessor2;

    public AdpDialogs(Activity a, ArrayList<ClsDialog> d) {
        activity = a;
        data = d;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mImageProcessor = new MaskImageProcessor(7);
        mImageProcessor2 = new ChainImageProcessor(
        		 new MaskImageProcessor(90),
                 new ScaleImageProcessor(25,50,ScaleType.FIT_XY));
    }

    public int getCount() {
        return data.size();
    }
    
    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
    	
        return position;
    }
    
    public static class ViewHolder{
        public TextView header,body,time;
        public AsyncImageView img,img2;
        public ImageView online,multiChat;
    }
    


    public View getView(int position, View convertView, ViewGroup parent) {
        View vi=convertView;
        final ViewHolder holder;
        
        if(convertView==null){
        	vi = inflater.inflate(R.layout.dialogs_row, null);
            holder=new ViewHolder();
            holder.header=(TextView)vi.findViewById(R.id.dlg_header);
            holder.body=(TextView)vi.findViewById(R.id.dlg_body);
            holder.time=(TextView)vi.findViewById(R.id.dlg_time);
            holder.online = (ImageView)vi.findViewById(R.id.dlg_online);
            holder.multiChat = (ImageView)vi.findViewById(R.id.dlg_multichat);
            holder.img = (AsyncImageView)vi.findViewById(R.id.user_thumb);
            holder.img.setImageProcessor(mImageProcessor);
            holder.img2 = (AsyncImageView)vi.findViewById(R.id.user_thumb2);
            holder.img2.setImageProcessor(mImageProcessor);
            vi.setTag(holder);
        }
        else
            holder=(ViewHolder) vi.getTag();
        
        ClsDialog o = data.get(position);
        if (o != null) {
        	ClsProfile p = o.getProfile();
        	String tmp = p.getFirst_name()+ " "+ p.getLast_name();
        	if (tmp.equals("")) tmp = p.getScreen_name();
        	if (o.getChat_id()!=0) {
        		tmp = o.getTitle();
        		holder.multiChat.setVisibility(View.VISIBLE);
        	}
        	else {
        		holder.multiChat.setVisibility(View.GONE);
        	}
        	holder.header.setText(tmp);
        	holder.body.setText(Html.fromHtml(o.getBody()));
        	holder.time.setText(Utils.convertTime(o.getDate()));
        	holder.img.setUrl(p.getPhoto());
        	if (p.getOnline()==1) {
        		holder.online.setVisibility(View.VISIBLE);
        	}
        	else holder.online.setVisibility(View.GONE);
        	if (p.getUid()!=AndroidApplication.getPrefsInt("user_id") && o.getOut()==1) {
        		holder.img2.setVisibility(View.VISIBLE);
        		holder.img2.setUrl(AndroidApplication.getPrefsString("photo"));
        	}
        	else {
        		holder.img2.setVisibility(View.GONE);
        	}
        }
        return vi;
    }
    
    

}

