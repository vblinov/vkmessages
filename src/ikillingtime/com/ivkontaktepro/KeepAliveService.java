package ikillingtime.com.ivkontaktepro;

import android.app.*;
import android.content.*;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class KeepAliveService extends IntentService//Service
{
	public KeepAliveService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	public static final String TAG = "KeepAliveService";

	private static final String HOST = "jasta.dyndns.org";
	private static final int PORT = 50000;

	private static final String ACTION_START = "ru.recoilme.vkmessages.keepalive.START";
	private static final String ACTION_STOP = "ru.recoilme.vkmessages.keepalive.STOP";
	private static final String ACTION_KEEPALIVE = "ru.recoilme.vkmessages.keepalive.KEEP_ALIVE";
	private static final String ACTION_RECONNECT = "ru.recoilme.vkmessages.keepalive.RECONNECT";
	
	private ConnectionLog mLog;
	private ConnectivityManager mConnMan;
	private NotificationManager mNotifMan;
	private boolean mStarted;
	private ConnectionThread mConnection;
	private static final long KEEP_ALIVE_INTERVAL = 1000 * 10;
	private static final long INITIAL_RETRY_INTERVAL = 1000 * 10;
	private static final long MAXIMUM_RETRY_INTERVAL = 1000 * 60 * 30;

	private SharedPreferences mPrefs;
	private static final int NOTIF_CONNECTED = 0;
	private static final String PREF_STARTED = "isStarted";
	
	private String longKey;
	private String longServer;
	private String longTs;

	public static void actionStart(Context ctx)
	{
		Intent i = new Intent(ctx, KeepAliveService.class);
		i.setAction(ACTION_START);
		ctx.startService(i);
	}

	public static void actionStop(Context ctx)
	{
		Intent i = new Intent(ctx, KeepAliveService.class);
		i.setAction(ACTION_STOP);
		ctx.startService(i);
	}
	
	public static void actionPing(Context ctx)
	{
		Intent i = new Intent(ctx, KeepAliveService.class);
		i.setAction(ACTION_KEEPALIVE);
		ctx.startService(i);
	}

	@Override
	public void onCreate()
	{
		super.onCreate();

		try {
			mLog = new ConnectionLog();
			Log.i(TAG, "Opened log at " + mLog.getPath());
		} catch (IOException e) {}

		mPrefs = getSharedPreferences(TAG, MODE_PRIVATE);
		
		mConnMan =
		  (ConnectivityManager)getSystemService(CONNECTIVITY_SERVICE);

		mNotifMan =
		  (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
	
		/* If our process was reaped by the system for any reason we need
		 * to restore our state with merely a call to onCreate.  We record
		 * the last "started" value and restore it here if necessary. */
		handleCrashedService();
	}
	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		//receiver.send(0, null);
		start();
	}
	
	private void handleCrashedService()
	{
		if (wasStarted() == true)
		{
			/* We probably didn't get a chance to clean up gracefully, so do
			 * it now. */
			hideNotification();			
			stopKeepAlives();

			/* Formally start and attempt connection. */
			start();
		}
	}
	
	@Override
	public void onDestroy()
	{
		log("Service destroyed (started=" + mStarted + ")");

		if (mStarted == true)
			stop();

		try {
			if (mLog != null)
				mLog.close();
		} catch (IOException e) {}
	}

	private void log(String message)
	{
		Log.i(TAG, message);

		if (mLog != null)
		{
			try {
				mLog.println(message);
			} catch (IOException e) {}
		}
	}
	
	private boolean wasStarted()
	{
		return mPrefs.getBoolean(PREF_STARTED, false);
	}

	private void setStarted(boolean started)
	{
		mPrefs.edit().putBoolean(PREF_STARTED, started).commit();
		mStarted = started;
	}

	//FIXME: deprecated
	@Override
	public void onStart(Intent intent, int startId)
	{
		log("Service started with intent=" + intent);

		super.onStart(intent, startId);

		if (intent.getAction().equals(ACTION_STOP) == true)
		{
			stop();
			stopSelf();
		}
		else if (intent.getAction().equals(ACTION_START) == true)
			start();
		else if (intent.getAction().equals(ACTION_KEEPALIVE) == true)
			keepAlive();
		else if (intent.getAction().equals(ACTION_RECONNECT) == true)
			reconnectIfNecessary();
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	private synchronized void start()
	{
		if (mStarted == true)
		{
			Log.w(TAG, "Attempt to start connection that is already active");
			return;
		}

		setStarted(true);

		registerReceiver(mConnectivityChanged,
		  new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

		log("Connecting...");
		if (getLongPoll()) {
			Log.d(TAG, ""+longKey+longServer+longTs);
			mConnection = new ConnectionThread(HOST, PORT);
			mConnection.start();
		}
	}

	private synchronized void stop()
	{
		if (mStarted == false)
		{
			Log.w(TAG, "Attempt to stop connection not active.");
			return;
		}

		setStarted(false);

		unregisterReceiver(mConnectivityChanged);		
		cancelReconnect();

		if (mConnection != null)
		{
			mConnection.abort();
			mConnection = null;
		}
	}

	private synchronized void keepAlive()
	{
		try {
			if (mStarted == true && mConnection != null)
				mConnection.sendKeepAlive();//.start();
		} catch (Exception e) {
			Log.e(TAG, e.toString());
		}
	}

	private void startKeepAlives()
	{
		Intent i = new Intent();
		i.setClass(this, KeepAliveService.class);
		i.setAction(ACTION_KEEPALIVE);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
		  System.currentTimeMillis() + KEEP_ALIVE_INTERVAL,
		  KEEP_ALIVE_INTERVAL, pi);
	}

	private void stopKeepAlives()
	{
		Intent i = new Intent();
		i.setClass(this, KeepAliveService.class);
		i.setAction(ACTION_KEEPALIVE);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmMgr.cancel(pi);
	}

	public void scheduleReconnect(long startTime)
	{
		long interval =
		  mPrefs.getLong("retryInterval", INITIAL_RETRY_INTERVAL);

		long now = System.currentTimeMillis();
		long elapsed = now - startTime;

		if (elapsed < interval)
			interval = Math.min(interval * 4, MAXIMUM_RETRY_INTERVAL);
		else
			interval = INITIAL_RETRY_INTERVAL;

		log("Rescheduling connection in " + interval + "ms.");

		mPrefs.edit().putLong("retryInterval", interval).commit();

		Intent i = new Intent();
		i.setClass(this, KeepAliveService.class);
		i.setAction(ACTION_RECONNECT);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmMgr.set(AlarmManager.RTC_WAKEUP, now + interval, pi);
	}
	
	public void cancelReconnect()
	{
		Intent i = new Intent();
		i.setClass(this, KeepAliveService.class);
		i.setAction(ACTION_RECONNECT);
		PendingIntent pi = PendingIntent.getService(this, 0, i, 0);
		AlarmManager alarmMgr = (AlarmManager)getSystemService(ALARM_SERVICE);
		alarmMgr.cancel(pi);
	}

	private synchronized void reconnectIfNecessary()
	{
		Log.d(TAG, "reconnectIfNecessary:"+mStarted);
		if (mStarted == true && mConnection == null)
		{
			log("Reconnecting...");
			
			mConnection = new ConnectionThread(HOST, PORT);
			mConnection.start();
		}
	}

	private BroadcastReceiver mConnectivityChanged = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			NetworkInfo info = (NetworkInfo)intent.getParcelableExtra
			  (ConnectivityManager.EXTRA_NETWORK_INFO);
			
			boolean hasConnectivity = (info != null && info.isConnected()) 
			  ? true : false;

			log("Connecting changed: connected=" + hasConnectivity);

			if (hasConnectivity)
				reconnectIfNecessary();
		}
	};
	
	private void showNotification(String msg)
	{
		Notification n = new Notification();
		
		n.flags = Notification.FLAG_NO_CLEAR |
		  Notification.FLAG_ONGOING_EVENT;

		n.icon = R.drawable.logo;
		n.when = System.currentTimeMillis();

		PendingIntent pi = PendingIntent.getActivity(this, 0,
		  new Intent(this, ActTabs.class), 0);
		n.setLatestEventInfo(this, "KeepAlive connected",
		 msg == null ? "Connected to " + HOST + ":" + PORT : msg, pi);

		mNotifMan.notify(NOTIF_CONNECTED, n);
	}
	
	private void hideNotification()
	{
		mNotifMan.cancel(NOTIF_CONNECTED);
	}
	
	private class ConnectionThread extends Thread
	{
		
		private final DefaultHttpClient longClient;

		public ConnectionThread(String host, int port)
		{
			HttpParams params = new BasicHttpParams();
			int timeout = 1000 * 60 * 60 * 24;
			params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
	        // Turn off stale checking.  Our connections break all the time anyway,
	        // and it's not worth it to pay the penalty of checking every time.
	        HttpConnectionParams.setStaleCheckingEnabled(params, false);

	        // Default connection and socket timeout of 10 seconds.  Tweak to taste.
	        HttpConnectionParams.setConnectionTimeout(params, 0);
	        HttpConnectionParams.setSoTimeout(params, 0);
	        HttpConnectionParams.setSocketBufferSize(params, 8192);
	        // Don't handle redirects -- return them to the caller.  Our code
	        // often wants to re-POST after a redirect, which we must do ourselves.
	        HttpClientParams.setRedirecting(params, false);
	        // Set the specified user agent and register standard protocols.
	        HttpProtocolParams.setUserAgent(params, "VK Messages");
	        SchemeRegistry schemeRegistry = new SchemeRegistry();
	        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
	        ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);
	        
	        longClient = new DefaultHttpClient(manager,params);
	        
		}
		
		public boolean isConnected()
		{
			return longClient==null?false:true;
		}
		
		private boolean isNetworkAvailable()
		{
			NetworkInfo info = mConnMan.getActiveNetworkInfo();
			if (info == null)
				return false;

			return info.isConnected();
		}

		public void run()
		{
			startKeepAlives();
			showNotification(null);
			

		}
		
		public void sendKeepAlive()
				  throws IOException
		{
			DefaultHttpClient localClient = longClient;
			long startTime = System.currentTimeMillis();
			HttpGet request = null;

			request = new HttpGet("http://"+longServer+"?act=a_check&key="+longKey+"&ts="+longTs+"&wait=25&mode=2");
			try {
				HttpResponse response = localClient.execute(request);
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					Log.d(TAG, "statusCode"+statusCode);
				}

				HttpEntity getResponseEntity = response.getEntity();

				if (getResponseEntity != null) {

					String s = EntityUtils.toString(getResponseEntity);
					Log.d(TAG, "Success:"+longTs+":"+s);
					//06-01 19:34:34.178: D/KeepAliveService(3900): Success:{"ts":1708751841,"updates":[]}
					JSONObject mJSONObject = new JSONObject(s);
			    	
			    	longTs = mJSONObject.optString("ts");
					

				}

			} 
			catch (Exception e) {
				if (request!=null) request.abort();
				e.printStackTrace();
			}
			
			log("Keep-alive sent.");
		}

		public void abort()
		{
			log("Connection aborting.");

		}
	}
/*
	private class ConnectionThread extends Thread
	{
		private final Socket mSocket;
		private final String mHost;
		private final int mPort;
		
		private volatile boolean mAbort = false;

		public ConnectionThread(String host, int port)
		{
			mHost = host;
			mPort = port;
			Log.d("rrrr", mHost);
			mSocket = new Socket();
		}

		public boolean isConnected()
		{
			return mSocket.isConnected();
		}

		private boolean isNetworkAvailable()
		{
			NetworkInfo info = mConnMan.getActiveNetworkInfo();
			if (info == null)
				return false;

			return info.isConnected();
		}

		public void run()
		{
			Socket s = mSocket;
			
			long startTime = System.currentTimeMillis();

			try {

				s.connect(new InetSocketAddress(mHost, mPort), 20000);
				
				// This is a special case for our demonstration.  The
				// keep-alive is sent from the client side but since I'm
				// testing it with just netcat, no response is sent from the
				// server.  This means that we might never actually read
				// any data even though our connection is still alive.  Most
				// instances of a persistent TCP connection would have some
				// sort of application-layer acknowledgement from the server
				// and so should set a read timeout of KEEP_ALIVE_INTERVAL 
				// plus an arbitrary timeout such as 2 minutes. 
				//s.setSoTimeout((int)KEEP_ALIVE_INTERVAL + 120000);

				log("Connection established to " + s.getInetAddress() +
				  ":" + mPort);
				
				startKeepAlives();
				showNotification(null);

				InputStream in = s.getInputStream();
				OutputStream out = s.getOutputStream();
				
				//Note that T-Mobile appears to implement an opportunistic
				// connect algorithm where the connect call may succeed
				// even when the remote peer would reject the connection.
				// Shortly after an attempt to send data an exception
				/ will occur indicating the connection was reset. 
				out.write("Hello, world.\n".getBytes());

				byte[] b = new byte[1024];
				int n;

				while ((n = in.read(b)) >= 0) {
					out.write(b, 0, n);
					showNotification(new String(b));
				}

				if (mAbort == false)
					log("Server closed connection unexpectedly.");
			} catch (IOException e) {
				log("Unexpected I/O error: " + e.toString());
			} finally {
				stopKeepAlives();
				hideNotification();

				if (mAbort == true)
					log("Connection aborted, shutting down.");
				else
				{
					try {
						s.close();
					} catch (IOException e) {}

					synchronized(KeepAliveService.this) {
						mConnection = null;
					}

					// If our local interface is still up then the connection
					// failure must have been something intermittent.  Try
					// our connection again later (the wait grows with each
					// successive failure).  Otherwise we will try to
					// reconnect when the local interface comes back. 
					if (isNetworkAvailable() == true)
						scheduleReconnect(startTime);
				}
			}
		}

		public void sendKeepAlive()
		  throws IOException
		{
			Socket s = mSocket;
			Date d = new Date();
			s.getOutputStream().write((d.toString() + "\n").getBytes());
			
			log("Keep-alive sent.");
		}

		public void abort()
		{
			log("Connection aborting.");

			mAbort = true;

			try {
				mSocket.shutdownOutput();
			} catch (IOException e) {}
			
			try {
				mSocket.shutdownInput();
			} catch (IOException e) {}
			
			try {
				mSocket.close();
			} catch (IOException e) {}

			while (true)
			{
				try {
					join();
					break;
				} catch (InterruptedException e) {}
			}
		}
	}*/
	
	private boolean getLongPoll() {
		String metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
		String code = "var messages = API.messages.getLongPollServer({});" +
        		"" +
        		"return {longpoll: messages};";
        String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
        
        HttpPost request = null;

		request = new HttpPost("http://api.vk.com"+metod+"&sig="+sig);
		Log.d(TAG,"getLongPoll");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("code", code));
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
			HttpResponse response = AndroidApplication.getClient().execute(request);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				return false;
			}

			HttpEntity getResponseEntity = response.getEntity();

			if (getResponseEntity != null) {

				String s = EntityUtils.toString(getResponseEntity);
				Log.d(TAG, ":"+s);

				JSONObject mJSONObject = new JSONObject(s);
		    	mJSONObject = mJSONObject.optJSONObject("response");
		    	mJSONObject = mJSONObject.optJSONObject("longpoll");
		    	longKey = mJSONObject.optString("key");
		    	longServer = Uri.decode(mJSONObject.optString("server"));
		    	longTs = mJSONObject.optString("ts");
				return true;
			}

		} 
		catch (Exception e) {
			if (request!=null) request.abort();
			e.printStackTrace();
		}
		return false;
	}

	
	
	
}
