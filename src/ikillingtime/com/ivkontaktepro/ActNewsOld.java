package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import android.widget.AbsListView.OnScrollListener;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class ActNewsOld extends Fragment {
    private static final String TAG = "ActNewsOld";
    private ArrayList<JSONObject> items;
    private ListView list;
    protected AQuery aq,listAq;
    private boolean isRunning=false;
    private Activity activity;
    private int offset = 0;
    private AdpNews adapter;
    private Map<String,String> groupsMap;
    private String new_offset="",new_from="";
    private ProgressBar progress;
    private Spinner spinner;
    private ArrayList<ClsAlbums> albums;
    public static String gid = "";
    private int cacheTime=0;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);

    }
    @Override
    public void onDestroy(){
        super.onDestroy();
    }
    public static void alert(Context context,String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(context);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);

        bld.create().show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.news, container, false);

        activity = getActivity();
        aq = new AQuery(activity);
        aq.hardwareAccelerated11();

        list = (ListView) v.findViewById(R.id.list);
        TextView emptyText = (TextView)v.findViewById(android.R.id.empty);
        list.setEmptyView(emptyText);
        progress = (ProgressBar) v.findViewById(R.id.progressBar);

        spinner = (Spinner) v.findViewById(R.id.spinner);
        items = new ArrayList<JSONObject>();
        groupsMap = new HashMap<String,String>();
        adapter = new AdpNews(activity,items,groupsMap);
        list.setAdapter(adapter);
        list.setOnScrollListener(onScroll);

        firstRequest();
        return v;
    }

    public void firstRequest() {
        new_offset = "";
        new_from="";
        Bundle params = new Bundle();
        params.putString("count", "1000");
        params.putString("extended", "1");

        isRunning = true;
        aq.progress(progress).ajax(generateUrl(params,"groups.get"), JSONObject.class,cacheTime, ActNewsOld.this, "onGroupsGet");

    }
    @Override
    public void onResume() {
        super.onResume();

    }

    public void onGroupsGet(String url, JSONObject json, AjaxStatus status) {
        isRunning = false;
        if(json == null) return;
        AQUtility.debug(json);
        JSONArray jsonArray = json.optJSONArray("response");
        if(jsonArray == null ) return;

        ArrayList<ClsAlbums> albums = new ArrayList<ClsAlbums>();
        ClsAlbums album = new ClsAlbums();
        album.setAlbum_id("");
        album.setOwner_id(AndroidApplication.getPrefsString("photo"));
        album.setTitle(getString(R.string.tab_news));
        albums.add(album);

        if (jsonArray.length()>1) {
            for (int i=1;i<jsonArray.length();i++) {
                JSONObject mJSONObject = jsonArray.optJSONObject(i);

                album = new ClsAlbums();
                album.setAlbum_id(mJSONObject.optString("gid"));
                album.setOwner_id(mJSONObject.optString("photo"));
                album.setTitle(mJSONObject.optString("name"));
                albums.add(album);
            }
        }

        AdpSpinner adpSpinner = new AdpSpinner(activity,albums);
        spinner.setAdapter(adpSpinner);
        if (AndroidApplication.getPrefsInt("currentSpinnerPosNews",0)<adpSpinner.getCount())
            spinner.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosNews",0));
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adpView, View arg1,int pos, long arg3)
            {
                AndroidApplication.putPrefsInt("currentSpinnerPosNews",pos);
                ClsAlbums album = (ClsAlbums) adpView.getItemAtPosition(pos);
                gid = album.getAlbum_id();
                new_offset = "";
                new_from = "";

                adapter.notifyDataSetChanged();
                request();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0){}
        });

    }

    public static String generateUrl(Bundle params,String method) {

        StringBuilder paramsString = new StringBuilder();
        SortedMap<String, String> paramsMap = new TreeMap<String, String>();
        for (String key : params.keySet()) {
            paramsMap.put(key, params.get(key).toString());
        }

        for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
        }
        if (method.equals("")) method = "execute";
        String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
        String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

        return  AndroidApplication.HOST+ methodString+ paramsString.toString()+"&sig="+sig;
    }

    private void request() {
        isRunning = true;
        Bundle params = new Bundle();
        params.putString("filters", "post");
        params.putString("count", "50");

        if (!gid.equals("")) {
            params.putString("source_ids", "g"+gid);
            //casheTime = -1;
        }
        if (!new_offset.equals("")) {
            params.putString("offset", Uri.encode(new_offset));
        }
        if (!new_from.equals("")) {
            params.putString("from", Uri.encode(new_from));
        }

        aq.progress(progress).ajax(generateUrl(params,"newsfeed.get"), JSONObject.class,cacheTime, ActNewsOld.this, "renderNews");
    }


    public void renderNews(String url, JSONObject json, AjaxStatus status) {

        if(json == null) return;
        AQUtility.debug(json);

        JSONObject o = json.optJSONObject("response");
        if (o==null) return;

        JSONArray ja = o.optJSONArray("items");
        if(ja == null) return;

        adapter.notifyDataSetInvalidated();
        //new_offset = o.optString("new_offset");
        if (new_from.equals("")) {
            items.clear();
        }
        new_from = o.optString("new_from").replace("#","");

        for(int i = 0 ; i < ja.length(); i++){
            JSONObject jo = ja.optJSONObject(i);
            items.add(jo);
        }

        JSONArray groups = o.optJSONArray("groups");

        if (groups!=null) {
            for(int j=0;j<groups.length();j++) {
                JSONObject g = groups.optJSONObject(j);
                groupsMap.put("-"+g.optString("gid"),g.optString("name"));
            }
        }
        //profiles":[{"uid":29652,"first_name":"��","sex":2,"last_name":"�����"
        JSONArray profiles = o.optJSONArray("profiles");

        if (profiles!=null) {
            for(int j=0;j<profiles.length();j++) {
                JSONObject p = profiles.optJSONObject(j);
                groupsMap.put(p.optString("uid"),p.optString("first_name")+" "+p.optString("last_name"));
            }
        }

        adapter.notifyDataSetChanged();

        if (cacheTime==0){
            cacheTime = AndroidApplication.cacheTime;
            if (cacheTime>0) {
                firstRequest();
            }
        }
        isRunning = false;
    }

    private OnScrollListener onScroll = new OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view,
                                         int scrollState) {

        }



        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount && !isRunning ) {
                request();
            }
        }

    };
}

