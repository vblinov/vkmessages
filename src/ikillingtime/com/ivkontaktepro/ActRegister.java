package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONObject;

import utils.asynctaskloader.AsyncTaskManager;
import utils.asynctaskloader.NetTask;
import utils.asynctaskloader.OnTaskCompleteListener;
import utils.serviceloader.RESTService;
import widget.MyActionBar;
import widget.ScrollingTextView;
import widget.MyActionBar.IntentAction;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ActRegister extends Activity implements OnFocusChangeListener{
    
	private static final String TAG = "C2DMReceiver";
	private ResultReceiver mReceiver;
	private EditText phone,name,lastname;
	private ImageView phoneimg,nameimg,lastnameimg; 
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
	    window.setFormat(PixelFormat.RGBA_8888);
	    window.addFlags(WindowManager.LayoutParams.FLAG_DITHER);
	    
        setContentView(R.layout.register);
        phone = (EditText) findViewById(R.id.singup_phone);
        name = (EditText) findViewById(R.id.singup_name);
        lastname = (EditText) findViewById(R.id.singup_lastname);
        phoneimg = (ImageView) findViewById(R.id.singup_phonecheck);
        nameimg = (ImageView) findViewById(R.id.singup_namecheck);
        lastnameimg = (ImageView) findViewById(R.id.singup_lastnamecheck);
        
        final ScrollingTextView title = (ScrollingTextView)findViewById(R.id.reg_title);
        title.setText(R.string.sgn_new);
        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
        };
        
        phone.addTextChangedListener(new TextWatcher(){
    	    public void afterTextChanged(Editable s){
    	    	if (s.toString().length()<5) {
    	    		phoneimg.setBackgroundResource(R.drawable.error);  		
    	    	}
    	    	else {
    	    		phoneimg.setBackgroundResource(R.drawable.ok);
    	    	}
    	    }
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
    	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
    	});
        name.addTextChangedListener(new TextWatcher(){
    	    public void afterTextChanged(Editable s){
    	    	int i=0;
    	    	try {
    	    		i = Integer.parseInt(s.toString());
    	    	} catch (Exception e) {}
    	    	if (s.toString().length()<1 || (""+i).equals(s.toString())) {
    	    		nameimg.setBackgroundResource(R.drawable.error);  		
    	    	}
    	    	else {
    	    		nameimg.setBackgroundResource(R.drawable.ok);
    	    	}
    	    }
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
    	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
    	});
        lastname.addTextChangedListener(new TextWatcher(){
    	    public void afterTextChanged(Editable s){
    	    	int i=0;
    	    	try {
    	    		i = Integer.parseInt(s.toString());
    	    	} catch (Exception e) {}
    	    	if (s.toString().length()<1 || (""+i).equals(s.toString())) {
    	    		lastnameimg.setBackgroundResource(R.drawable.error);  		
    	    	}
    	    	else {
    	    		lastnameimg.setBackgroundResource(R.drawable.ok);
    	    	}
    	    }
    	    public void beforeTextChanged(CharSequence s, int start, int count, int after){ }
    	    public void onTextChanged(CharSequence s, int start, int before, int count){ }
    	});
    }

	public void onRESTResult(int code, String result, int mode) {
     	Log.d(TAG, "onRESTResult:"+result);
     	if (code==200) {
     		if (mode==0) {
     			
     			if (result.contains("error_msg")) {
     				try {
     					JSONObject o = new JSONObject(result);
     					o = o.optJSONObject("error");
     					Toast.makeText(this, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
     				}
     				catch (Exception e) {}
     				return;
     			}
     			//no error
     			Toast.makeText(this, R.string.cnf_sms, Toast.LENGTH_LONG).show();
     			ActConfirm.phoneNumber = phone.getText().toString();
     			startActivity(ActConfirm.createIntent(ActRegister.this));
     		}
     	}
	}
	
	private String restRequest(Bundle params,Boolean useCashe,int httpVerb,int mode,String method) {

    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method;//+"?access_token="+AndroidApplication.getPrefsString("access_token");
		//String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
		
        Intent intent = new Intent(this, RESTService.class);
        intent.setData(Uri.parse("https://api.vk.com/"+methodString));//+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        startService(intent);
        return "";
	}
	
    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActRegister.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		// TODO Auto-generated method stub
		Log.d(TAG, v.toString());
	}
	
	public void onRegister(View v) {
		Bundle params = new Bundle();
	    params.putString("phone", phone.getText().toString());
	    params.putString("first_name", name.getText().toString());
	    params.putString("last_name", lastname.getText().toString());
	    params.putString("client_id", "2965041");
	    //params.putString("test_mode", "1");//dont forget comment this, Luke!
	    params.putString("client_secret", "fXK28HAI0nVRK3hNZiGs");
    	restRequest(params,false,2,0,"auth.signup");	
	}
	public void onBack(View v) {
		finish();
	}
}