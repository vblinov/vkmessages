package ikillingtime.com.ivkontaktepro;

import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.serviceloader.RESTService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.android.c2dm.C2DMBaseReceiver;

public class C2DMReceiver extends C2DMBaseReceiver {
	private static final String TAG = "C2DMReceiver";
	private ResultReceiver mReceiver;
	private Context mContext;
	private String txt="";
	
	public C2DMReceiver(){
        super("vadim.kulibaba@gmail.com");
        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
        };
    }
	
	 public void onRESTResult(int code, String result, int mode) {
     	Log.d(TAG, "onRESTResult:"+result);
     	if (mode==18) {
     		String title = mContext.getString(R.string.app_name);
     		try{
     			//{"response":[{"uid":"1","first_n
     			JSONObject mJSONObject = new JSONObject(result);
     			JSONArray r = mJSONObject.optJSONArray("response");
     			mJSONObject = r.getJSONObject(0);
     			title = mJSONObject.optString("first_name")+" "+mJSONObject.optString("last_name");
     			
     		}
     		catch (Exception e) {}
     		
     		Intent intent = new Intent(mContext,ActTabs.class);
     		//intent.putExtra("msg_id", data.getStringExtra("msg_id"));
     		NotificationManager mManager = (NotificationManager) 
		                getSystemService(Context.NOTIFICATION_SERVICE);
		            Notification notification = new Notification(R.drawable.icon_i,
		            		title, System.currentTimeMillis());
		            notification.setLatestEventInfo(mContext,mContext.getString(R.string.app_name),title+": "+txt,
		                PendingIntent.getActivity(this.getBaseContext(), 0, 
		                    intent,PendingIntent.FLAG_CANCEL_CURRENT));
		            mManager.notify(0, notification);
     	}
	 }
	
	@Override
    public void onRegistered(Context context, String registrationId) {
        Log.d(TAG,"onRegistered"+registrationId);

    	Bundle params = new Bundle();
	    params.putString("token", registrationId);
	    restRequest(params,false,2,3,"account.registerDevice");
    }
	
	private String restRequest(Bundle params,Boolean useCashe,int httpVerb,int mode,String method) {

    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
		
        Intent intent = new Intent(this, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        startService(intent);
        return sig;
	}
	@Override
    public void onUnregistered(Context context) {
		Log.d(TAG,"onUnregistered"+ "");
    }
	
	@Override
	protected void onMessage(Context context, Intent data) {
		// TODO Auto-generated method stub
		Log.d(TAG,"onMessage"+ "");

        if(data != null)
        {
        	String collapse_key = ""+data.getStringExtra("collapse_key");
            Log.d("C2DMReceiver", collapse_key);
            try {
            if (collapse_key.equals("vkmsg")) {
            	
	            
	            //intent.putExtra("msg_id", data.getStringExtra("msg_id"));
                String userid = ""+AndroidApplication.getPrefsInt("user_id");
                String current_uid = ""+AndroidApplication.getPrefsInt("current_uid");
                String uid = ""+data.getStringExtra("uid");

	            if (!(userid).equals(uid) &&
	            		!current_uid.equals(uid)) {
	            	mContext = context;
	            	txt = ""+data.getStringExtra("text");
	            	Bundle params = new Bundle();
	        	    params.putString("uids", ""+data.getStringExtra("uid"));
	        	    restRequest(params,false,2,18,"users.get");
	            }
            }
            if (collapse_key.equals("vkfriend")) {
            	mContext = context;
            	String title = mContext.getString(R.string.app_name);
            	txt = mContext.getString(R.string.srh_friendrequests);
            	Intent intent = new Intent(mContext,ActTabs.class);
         		//intent.putExtra("msg_id", data.getStringExtra("msg_id"));
         		NotificationManager mManager = (NotificationManager) 
    		                getSystemService(Context.NOTIFICATION_SERVICE);
    		            Notification notification = new Notification(R.drawable.icon_i,
    		            		title, System.currentTimeMillis());
    		            notification.setLatestEventInfo(mContext,mContext.getString(R.string.app_name),txt,
    		                PendingIntent.getActivity(this.getBaseContext(), 0, 
    		                    intent,PendingIntent.FLAG_CANCEL_CURRENT));
    		            mManager.notify(0, notification);
            }
            }
            catch (Exception e) {
                Log.e(TAG,e.toString());
            }
            
        }
	}

	@Override
	public void onError(Context context, String errorId) {
		// TODO Auto-generated method stub
		Log.d(TAG,"onError"+ errorId);
	}

}
