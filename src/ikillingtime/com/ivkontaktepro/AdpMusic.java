package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 10.06.13
 * Time: 11:02
 * To change this template use File | Settings | File Templates.
 */
public class AdpMusic extends BaseAdapter {

    private Activity activity;
    private ArrayList<ClsMusic> data;
    private LayoutInflater inflater=null;
    protected AQuery listAq;
    private ActAbstractGroup actAbstractGroup;

    public AdpMusic(Activity a, ArrayList<ClsMusic> d,ActAbstractGroup f) {
        activity = a;
        data = d;
        actAbstractGroup = f;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listAq = new AQuery(activity);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = activity.getLayoutInflater().inflate(R.layout.music_row, null);
        }

        AQuery aq = listAq.recycle(view);
        final ClsMusic music = data.get(i);
        aq.id(R.id.tracklist_item_title).text((i+1)+". "+music.getTitle());
        aq.id(R.id.tracklist_item_artist).text(music.getArtist());
        aq.id(R.id.tracklist_item_duration).text(music.getDurationStr());

        if (i==ActMusic.listPosition){
            aq.id(R.id.tracklist_item).background(R.drawable.list_selector);
        }
        else {
            aq.id(R.id.tracklist_item).backgroundColor(Color.TRANSPARENT);
        }
        final View more = aq.id(R.id.button_more).clickable(true).getView();
        final int width = view.getWidth();
        more.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showPopup(more,music,width);
                }
            });


        return view;
    }

    private void showPopup(View anchorView,final ClsMusic o,int width) {

        List<String> menuMusic = new ArrayList<String>();
        menuMusic.add(activity.getString(R.string.tab_search)+": "+o.getArtist());
        menuMusic.add(!AndroidApplication.getPrefsString("CURMUSGID").equals("")?
                activity.getString(R.string.music_actlist1):
                activity.getString(R.string.music_actlist2));


        final ListPopupWindow
                popup = new ListPopupWindow(activity);
        popup.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1,menuMusic.toArray(new String[menuMusic.size()]) ));
        popup.setAnchorView(anchorView);
        popup.setModal(true);
        popup.setWidth(Math.max(400, width));

        popup.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                popup.dismiss();

                if (position == 0) {
                    actAbstractGroup.doSearch(o.getArtist());
                }
                if (position == 1) {

                    if (o.getUrl().toLowerCase().contains("http")) {
                        Bundle params = new Bundle();
                        params.putString((!AndroidApplication.getPrefsString("CURMUSGID").equals("")) ? "audio_id" : "album_id", ""+o.getAid());
                        params.putString("owner_id", ""+o.getOwner_id());
                        listAq.ajax(ActNewsOld.generateUrl(params, (!AndroidApplication.getPrefsString("CURMUSGID").equals("")) ? "audio.add" : "audio.delete"), JSONObject.class,
                                1, AdpMusic.this, "onAction");
                    }
                    else {
                        File f = new File(o.getUrl());
                        boolean result = f.delete();
                        if (result) {
                            Toast.makeText(activity,activity.getString(R.string.music_deletefromsd),Toast.LENGTH_SHORT).show();
                            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
                            actAbstractGroup.onBrowse();
                        }
                    }
                }
            }
        });
        popup.show();
    }

    public void onAction(String url, JSONObject json, AjaxStatus status) {
        if (json!=null && !json.toString().contains("error_msg")) {
            if (url.contains("audio.delete")) {
                actAbstractGroup.onBrowse();
            }
            if (url.contains("audio.add"))
                ActNewsOld.alert(activity, "OK");
        }
        else {
            try {
                JSONObject o = json.optJSONObject("error");
                ActNewsOld.alert(activity, "Error:" + o.optString("error_msg"));
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
