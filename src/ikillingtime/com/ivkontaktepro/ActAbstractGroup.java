package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.SortedMap;
import java.util.TreeMap;

public abstract class ActAbstractGroup extends Fragment {
    public static final String TAG = "ActAbstractGroup";
    public static final int MODE_GETALL = 0;
    private static final int MODE_GETALBUMS = 5;
    public static final int MODE_GETALBUM_CONTENT = 4;
    public static final int MODE_SEARCH = 2;
    public static final int MODE_SDCART = 6;
    public static final int COUNT_MEDIUM = 100;
    private static final int CAPTCHA_RESULT = 0;
    private ArrayList<ClsAlbums> albums;
    public int dlgsSize;
    public String searchQuery="";
    private boolean paused = false;
    public boolean clearList = true;
    public boolean isRunning=false;
    public Activity activity;
    public GridView list;
    public int offset;
    public ProgressBar progress;
    //TODO make private with getter setter
    private int MODE_CURR=0;
    private EditText searchTxt;
    private Dialog dialog = null;
    protected AQuery aq;
    private Spinner spinner,spinnerAlb;
    private String gid;
    public String aid;
    private LinearLayout searchPanel;
    public String viewved = "";


    public ArrayList dlgs;
    public BaseAdapter adapter;
    public String my_string;
    public SeekBar trackSeek;
    public TextView currentTrackProgressView, currentTrackDurationView;
    public ImageButton playButton, prevButton, nextButton, shuffleButton;
    public LinearLayout playerControls,dialogs;
    public int customSpinner = 0;
    private int cacheTime=0;

    public void setAlbums(ArrayList<ClsAlbums> _albums) {
        albums = _albums;
    }
    public String getGid() {
        return this.gid;
    }

    public void setGid(String gid){
        this.gid = gid;
    }

    public int getCacheTime() {
        return this.cacheTime;
    }

    public void setCacheTime(int cacheTime){
        this.cacheTime = cacheTime;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = getActivity();
        setCacheTime(0);
        initAdapter();
    }

    public abstract void initAdapter();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.abstract_group, container, false);

        searchTxt = (EditText) v.findViewById(R.id.dlg_searchtxt);
        spinner = (Spinner) v.findViewById(R.id.spinner);
        spinnerAlb = (Spinner) v.findViewById(R.id.spiner);

        progress = (ProgressBar)v.findViewById(R.id.progressBar);
        trackSeek = (SeekBar)v.findViewById(R.id.SeekBar01);
        currentTrackProgressView = (TextView)v.findViewById(R.id.txtStart);
        currentTrackDurationView = (TextView)v.findViewById(R.id.txtEnd);
        playButton = (ImageButton)v.findViewById(R.id.btnPlay);
        playerControls = (LinearLayout)v.findViewById(R.id.keyboardhidelayout);
        dialogs = (LinearLayout)v.findViewById(R.id.dialogs);
        prevButton = (ImageButton)v.findViewById(R.id.btnRew);
        nextButton = (ImageButton)v.findViewById(R.id.btnFf);
        shuffleButton = (ImageButton)v.findViewById(R.id.btnSfl);
        playerControls.setVisibility(View.VISIBLE);

        list = (GridView) v.findViewById(R.id.grid);


        aq = new AQuery(activity);

        list.setAdapter(adapter);
        listListeners();


        AnimationSet set = new AnimationSet(true);
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(400);
        set.addAnimation(animation);
        animation = new TranslateAnimation(
            Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
            Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(400);
        set.addAnimation(animation);

        LayoutAnimationController controller =
            new LayoutAnimationController(set, 0.25f);
        list.setLayoutAnimation(controller);


        searchTxt.setText("");
        searchTxt.setOnKeyListener(onSchTxt);

        searchPanel = (LinearLayout) v.findViewById(R.id.search_panel);



        final Button srchBtn = (Button) v.findViewById(R.id.search_icon);
        srchBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation.AnimationListener listenerOnShowPanel = new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if (searchTxt.getVisibility() == View.GONE) {
                            if (spinnerAlb.getVisibility() == View.VISIBLE) {
                                spinnerAlb.setVisibility(View.GONE);
                            }
                            searchTxt.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, searchTxt, false));
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                };
                if (searchPanel.getVisibility() == View.VISIBLE) {
                    if (searchTxt.getVisibility() == View.GONE) {
                        if (spinnerAlb.getVisibility() == View.VISIBLE) {
                            spinnerAlb.setVisibility(View.GONE);
                        }
                        searchTxt.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, searchTxt, false));
                    }
                    else {
                        searchPanel.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 1.0f, 0.0f, 500, searchPanel, true));
                    }
                }
                else {
                    //show panel & edit text
                    Animation panelAnimation = new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, searchPanel, false);
                    panelAnimation.setAnimationListener(listenerOnShowPanel);
                    searchPanel.startAnimation(panelAnimation);

                }
            }
        });

        firstRequest();
        return v;
    }

    public void firstRequest() {
        isRunning = true;
        dlgsSize = 0;
        offset = 0;

        Bundle params = new Bundle();
        params.putString("count", "1000");
        params.putString("extended", "1");
        aq.progress(progress).ajax(ActNewsOld.generateUrl(params, "groups.get"), JSONObject.class,
                cacheTime, ActAbstractGroup.this, "onGroupsGet");

    }

    public abstract void listListeners();

    public void onGroupsGet(String url, JSONObject json, AjaxStatus status) {
        isRunning = false;
        if(json == null) return;

        JSONArray jsonArray = json.optJSONArray("response");
        if(jsonArray == null ) return;

        ArrayList<ClsAlbums> albums = new ArrayList<ClsAlbums>();
        ClsAlbums album = new ClsAlbums();
        album.setAlbum_id("");
        album.setOwner_id(AndroidApplication.getPrefsString("photo"));
        album.setTitle(my_string);
        albums.add(album);
        if (customSpinner==1) {
            album = new ClsAlbums();
            album.setAlbum_id("");
            album.setOwner_id(AndroidApplication.getPrefsString("photo"));
            album.setTitle(getString(R.string.music_sdcart));
            albums.add(album);
        }
        if (customSpinner==2) {
            album = new ClsAlbums();
            album.setAlbum_id("u"+AndroidApplication.getPrefsInt("user_id",0));
            album.setOwner_id(AndroidApplication.getPrefsString("photo"));
            album.setTitle(getString(R.string.news_friends));
            albums.add(album);
        }

        if (jsonArray.length()>1) {
            for (int i=1;i<jsonArray.length();i++) {
                JSONObject mJSONObject = jsonArray.optJSONObject(i);

                album = new ClsAlbums();
                album.setAlbum_id(mJSONObject.optString("gid"));
                album.setOwner_id(mJSONObject.optString("photo"));
                album.setTitle(mJSONObject.optString("name"));
                albums.add(album);
            }
        }

        AdpSpinner adpSpinner = new AdpSpinner(activity,albums);
        spinner.setAdapter(adpSpinner);
        if (customSpinner==1){
            //music
            if (AndroidApplication.getPrefsInt("currentSpinnerPosMusic",0)<adpSpinner.getCount())
                spinner.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosMusic",0));
        }
        else {
            //video
            if (customSpinner == 0) {
                if (AndroidApplication.getPrefsInt("currentSpinnerPosVideo",0)<adpSpinner.getCount()) {
                    spinner.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosVideo",0));
                    AQUtility.debug(TAG,"currentSpinnerPosVideo"+AndroidApplication.getPrefsInt("currentSpinnerPosVideo",0));
                }
            }
            //news
            if (customSpinner == 2) {
                if (AndroidApplication.getPrefsInt("currentSpinnerPosNews",0)<adpSpinner.getCount()) {
                    spinner.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosNews",0));
                    AQUtility.debug(TAG,"currentSpinnerPosNews"+AndroidApplication.getPrefsInt("currentSpinnerPosNews",0));
                }
            }
        }
        spinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adpView, View arg1,int pos, long arg3)
            {
                if (customSpinner==1){
                    AndroidApplication.putPrefsInt("currentSpinnerPosMusic",pos);

                }
                else {
                    if (customSpinner == 0) {
                        AndroidApplication.putPrefsInt("currentSpinnerPosVideo",pos);
                    }
                    //news
                    if (customSpinner == 2) {
                        AndroidApplication.putPrefsInt("currentSpinnerPosNews",pos);
                    }
                }
                if (pos==0) {
                    offset = 0;
                    clearList = true;
                    setGid("");
                    MODE_CURR = MODE_GETALBUMS;
                    getAlbums();
                }
                else {
                    if (customSpinner==1 && pos==1) {
                       MODE_CURR = MODE_SDCART;

                       getSdCartMusic();
                    }
                    else {
                        ClsAlbums album = (ClsAlbums) adpView.getItemAtPosition(pos);
                        setGid(album.getAlbum_id());
                        viewved = AndroidApplication.getPrefsString("v_"+getGid());
                        MODE_CURR = MODE_GETALBUMS;
                        getAlbums();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0){}
        });

    }

    public int getCurrentMode () {
        return this.MODE_CURR;
    }

    public abstract void getAlbums();

    public abstract void getSdCartMusic();

    private View.OnKeyListener onSchTxt = new View.OnKeyListener() {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {

            if (event.getAction() == KeyEvent.ACTION_DOWN)
            {
                switch (keyCode)
                {
                    case KeyEvent.KEYCODE_DPAD_CENTER:
                    case KeyEvent.KEYCODE_ENTER:

                        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        doSearch(searchTxt.getText().toString());

                        return true;
                    default:
                        break;
                }
            }
            return false;

        }

    };

    public void doSearch(String s){
        searchQuery = s;
        MODE_CURR = MODE_SEARCH;
        clearList = true;
        offset = 0;
        onBrowse();
    }

    public void increaseOffset() {
        offset = offset + COUNT_MEDIUM;
    }

    public void request(Bundle params, int httpVerb, String method) {
        isRunning = true;

        StringBuilder paramsString = new StringBuilder();
        SortedMap<String, String> paramsMap = new TreeMap<String, String>();
        for (String key : params.keySet()) {
            paramsMap.put(key, params.get(key).toString());
        }

        for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
        }
        if (method.equals("")) method = "execute";
        String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
        String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

        String url = AndroidApplication.HOST+methodString+"&sig="+sig;

        String onResult = "onRequest";
        if (httpVerb==-1) {
            //POST
            AQUtility.debug("post");
            aq.progress(progress).ajax(url,paramsMap, String.class,  ActAbstractGroup.this, onResult);
        }
        else
            aq.progress(progress).ajax(AndroidApplication.HOST + methodString + paramsString.toString() + "&sig=" + sig, String.class, cacheTime, ActAbstractGroup.this, onResult);
    }

    public void onRequest(String url, String res, AjaxStatus status) {
        AQUtility.debug(TAG,"url"+url);
        if (status.getCode()==200)
            onRESTResult(status.getCode(),""+res,MODE_CURR);
        else {
            if (status!=null && status.getError()!=null)
                AQUtility.debug("Err",status.getError());
        }
    }

    private void onRESTResult(int code, String result, int mode) {
        AQUtility.debug(TAG, "cachetime" + cacheTime + "onRestResult:" + result);
        if (paused == true) return;
        if (code == 200 && result != null) {

            if (result.contains("error_msg")) {
                try {
                    JSONObject o = new JSONObject(result);
                    o = o.optJSONObject("error");
                    Toast.makeText(activity, "Error:"+o.optString("error_msg"), Toast.LENGTH_LONG).show();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (result.contains("error_code\":14")) {
                    //parseCaptcha(result);
                    Intent intent = new Intent(activity, ActCaptcha.class);
                    intent.putExtra("result", result);
                    startActivityForResult(intent, CAPTCHA_RESULT);
                    return;
                }
                return;
            }
            switch(mode) {
            case MODE_GETALL:
                parseResult(result);
                //if (cacheTime>0) offset+=COUNT_MEDIUM;
                break;
            case MODE_SEARCH:
                parseResult(result);

               // if (cacheTime>0) offset+=COUNT_MEDIUM;
                break;
            case MODE_GETALBUM_CONTENT:
                parseResult(result);

                //if (cacheTime>0) offset+=COUNT_MEDIUM;
                break;
            case MODE_GETALBUMS:

                parseAlbums(result);
                break;
            }
        }
        else {

        }
    }

    private void parseAlbums(String result) {
        albums = new  ArrayList<ClsAlbums>();
        ClsAlbums album = new ClsAlbums();
        album.setAlbum_id("");
        album.setOwner_id("");
        album.setTitle(getString(R.string.vid_allalbums));
        albums.add(album);
        try {
            JSONObject mJSONObject = new JSONObject(result.toString());
            //mJSONObject = mJSONObject.optJSONObject("response");
            JSONArray mArr = mJSONObject.optJSONArray("response");

            if (mArr.optInt(0)!=0) {
                for (int i=1;i<mArr.length();i++) {
                    mJSONObject = mArr.getJSONObject(i);
                    album = new ClsAlbums();

                    album.setAlbum_id(mJSONObject.optString("album_id"));
                    album.setOwner_id(mJSONObject.optString("owner_id"));
                    album.setTitle(mJSONObject.optString("title"));
                    albums.add(album);
                }
            }


        }
        catch (Exception e) {
            Log.e(TAG,e.toString());
        }
        String[] titles = new String[albums.size()];
        int i = 0;
        for (ClsAlbums a: albums) {
            titles[i] = a.getTitle();

            i++;
        }

        setupSpinnerAlbum(titles);
    }

    public void setupSpinnerAlbum(String[] titles) {
        ArrayAdapter<String> albums_arr = new ArrayAdapter<String>(activity,android.R.layout.simple_spinner_item, titles);
        albums_arr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //animation
        Animation.AnimationListener listenerOnShowPanel = new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (spinnerAlb.getVisibility() == View.GONE) {
                    if (searchTxt.getVisibility() == View.VISIBLE) {
                        searchTxt.setVisibility(View.GONE);
                    }
                    spinnerAlb.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, spinnerAlb, false));
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };
        if (searchPanel.getVisibility() == View.VISIBLE) {
            if (spinnerAlb.getVisibility() == View.GONE) {
                if (searchTxt.getVisibility() == View.VISIBLE) {
                    searchTxt.setVisibility(View.GONE);
                }
                spinnerAlb.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, spinnerAlb, false));
            }

        }
        else {
            //show panel & edit text
            Animation panelAnimation = new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, searchPanel, false);
            panelAnimation.setAnimationListener(listenerOnShowPanel);
            searchPanel.startAnimation(panelAnimation);

        }
        spinnerAlb.setAdapter(albums_arr);
        if (customSpinner==1){
            //music
            if (AndroidApplication.getPrefsInt("currentSpinnerPosMusicAlbum"+getGid()+MODE_CURR,0)<albums_arr.getCount())
                spinnerAlb.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosMusicAlbum"+getGid()+MODE_CURR,0));
            }
        else {
            //video
            if (customSpinner==0){
                if (AndroidApplication.getPrefsInt("currentSpinnerPosVideoAlbum"+getGid(),0)<albums_arr.getCount()) {
                    spinnerAlb.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosVideoAlbum"+getGid(),0));

                }
            }
            //news
            if (customSpinner==2){
                if (AndroidApplication.getPrefsInt("currentSpinnerPosNewsAlbum"+getGid(),0)<albums_arr.getCount()) {
                    spinnerAlb.setSelection(AndroidApplication.getPrefsInt("currentSpinnerPosNewsAlbum"+getGid(),0));

                }
            }
        }
        spinnerAlb.setOnItemSelectedListener(new Spinner.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adpView, View arg1,int pos, long arg3)
            {
                if (customSpinner==1){
                    AndroidApplication.putPrefsInt("currentSpinnerPosMusicAlbum"+getGid()+MODE_CURR,pos);
                }
                else {
                    //video
                    if (customSpinner==0){
                        AndroidApplication.putPrefsInt("currentSpinnerPosVideoAlbum"+getGid(),pos);
                    }
                    //news
                    if (customSpinner==2){
                        AndroidApplication.putPrefsInt("currentSpinnerPosNewsAlbum"+getGid(),pos);
                    }
                }
                if (MODE_CURR == MODE_SDCART){
                    processCustomAlbumSpinner(adpView,pos);
                }
                else {

                    if (albums==null || albums.size()<=pos)
                        return;
                    aid = albums.get(pos).getAlbum_id();
                    paused = false;
                    offset = 0;
                    clearList = true;
                    MODE_CURR=MODE_GETALBUM_CONTENT;
                    onBrowse();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0){}
        });
    }
    public abstract void parseResult(String result);
    public abstract void processCustomAlbumSpinner(AdapterView<?> adpView,int pos);

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AQUtility.debug("ActAbsGr",requestCode+"|"+data);

        Bundle params = null;
        if (data!=null && data.hasExtra(ActCaptcha.EXTRA_CAPTCHA_PARAMS)) {
            params = data.getExtras().getParcelable(ActCaptcha.EXTRA_CAPTCHA_PARAMS);
        }
        else {
            params = ActCaptcha.result;
        }
        AQUtility.debug("params",params);
        if(params!=null && requestCode==CAPTCHA_RESULT){

            String apiMetod = "";
            Bundle paramss = new Bundle();

            for (String key : params.keySet()) {
                String value = params.get(key).toString();
                if (key.equals("method")) {
                    apiMetod = value;
                }
                else {
                    paramss.putString(key, value);
                }
            }
            request(paramss, 2, apiMetod);
            return;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        paused = false;
    }
    @Override
    public void onPause() {
        super.onPause();
        paused = true;
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    public abstract void onBrowse();
}