package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import com.google.android.c2dm.C2DMessaging;
import org.json.JSONArray;
import org.json.JSONObject;
import utils.serviceloader.RESTService;

public class ActLogin extends Activity implements OnFocusChangeListener{
    
	private static final String TAG = "ActLogin";
	private ImageView phone, pass;
	private EditText phoneTxt, passTxt;
	private ResultReceiver mReceiver;
	private Activity activity;
	private TextView login_register,login_registerhint;
    private static final int CAPTCHA_RESULT = 0;
    private Button loginBtn;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
	    window.setFormat(PixelFormat.RGBA_8888);
	    window.addFlags(WindowManager.LayoutParams.FLAG_DITHER);
        setContentView(R.layout.login);
        phone = (ImageView) findViewById(R.id.phone);
        pass = (ImageView) findViewById(R.id.pass);
        phoneTxt = (EditText) findViewById(R.id.inputloginr);
        passTxt = (EditText) findViewById(R.id.inputloginl);
        login_register = (TextView) findViewById(R.id.login_register);
        login_registerhint = (TextView) findViewById(R.id.login_registerhint);
        loginBtn = (Button) findViewById(R.id.loginBtn);

        phoneTxt.setText(AndroidApplication.getPrefsString("uL"));
        phoneTxt.setOnFocusChangeListener(this);
        passTxt.setOnFocusChangeListener(this);
        login_registerhint.setOnFocusChangeListener(this);
        loginBtn.setEnabled(true);

        //fXK28HAI0nVRK3hNZiGs
    	activity = this;
    	mReceiver = new ResultReceiver(new Handler()) {
             @Override
             protected void onReceiveResult(int resultCode, Bundle resultData) {
                 if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                     onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                 }
                 else {
                     onRESTResult(resultCode, null,0);
                 }
             }
             
        };
    }
	
	public void onRESTResult(int code, String result, int mode) {
		Log.d(TAG, "onRESTResult:"+result);
		if (code == 200 && result != null) {
        	
        	if (result.contains("error\":10")) {
        		Toast.makeText(activity, "Error: "+result, Toast.LENGTH_LONG).show();
		        return;
        	}
        	switch (mode) {
        	case 0:

        		onLoginResult(result);
        		break;
        	case 1:
        		try
                {
                    AndroidApplication.putPrefsInt("haswall", 1);
            		JSONObject mJSONObject = new JSONObject(result);
            		mJSONObject = mJSONObject.optJSONObject("response");
            		if (mJSONObject!=null) {
	            		JSONArray mJSONArray = mJSONObject.optJSONArray("user");
	            		if (mJSONArray!=null && mJSONArray.length()>0) {
	            			mJSONObject = mJSONArray.optJSONObject(0);
	            			AndroidApplication.putPrefsString("fio", mJSONObject.optString("first_name")+" "+mJSONObject.optString("last_name"));
	            			AndroidApplication.putPrefsString("photo", Uri.decode(mJSONObject.optString("photo")));
	            		}
            		}
                }
            	catch (Exception e)
                {
            		Toast.makeText(this,
        					   e.getMessage(),
        					   Toast.LENGTH_LONG).show();
                }
                C2DMessaging.register(activity.getApplicationContext(), "vadim.kulibaba@gmail.com");
                
        		startActivity(ActTabs.createIntent(ActLogin.this)); 
        		break;
        	}
        }
		else if (code == 401) {
			Toast.makeText(activity, R.string.lgn_wrong, Toast.LENGTH_LONG).show();
		}
		else {
			Toast.makeText(activity, R.string.dlg_errupload, Toast.LENGTH_LONG).show();
		}
	}

	private void restRequestHttps(String code,int mode) {

        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(code));
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x1);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
	}
	
	private String restRequest(String code,int mode) {
		String metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));

		Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+metod+"&sig="+sig));
        Bundle params = new Bundle();
        params.putString("code", code);
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, 0x2);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
    
    public void onRegister(View v) {
    	login_register.setTextColor(getResources().getColor(R.color.white));
    	login_registerhint.setTextColor(getResources().getColor(R.color.white));
    	startActivity(ActRegister.createIntent(ActLogin.this));
    }
    
    public void onLogin(View v) {
    	if (checkFields()) {
            Toast.makeText(ActLogin.this,"Please wait...",Toast.LENGTH_LONG).show();
            AndroidApplication.putPrefsString("uL",phoneTxt.getText().toString().trim());
            AndroidApplication.putPrefsString("uP",passTxt.getText().toString().trim());
    		//restRequestHttps("https://api.vk.com/oauth/token?grant_type=password&client_id=2965041&client_secret=fXK28HAI0nVRK3hNZiGs&username="+
    		//		Uri.encode(phoneTxt.getText().toString().trim())+"&password="+Uri.encode(passTxt.getText().toString())+"&scope=friends,messages,notifications,offline,nohttps,photos,audio,video,docs,notify,notes,status,groups,wall",0);
            AQuery aq = new AQuery(activity);
            aq.ajax("https://api.vk.com/oauth/token?grant_type=password&client_id=2965041&client_secret=fXK28HAI0nVRK3hNZiGs&username="+
                    Uri.encode(phoneTxt.getText().toString().trim())+"&password="+Uri.encode(passTxt.getText().toString())+"&scope=friends,messages,notifications,offline,nohttps,photos,audio,video,docs,notify,notes,status,groups,wall",
                    String.class, -1, ActLogin.this, "onLogin");
            loginBtn.setEnabled(false);
        }
    }

    public void onLogin(String url, String result, AjaxStatus status) {
        loginBtn.setEnabled(true);
        if (status!=null && status.getError()!=null && status.getError().contains("captcha")) {
            //parseCaptcha(result);
            Intent intent = new Intent(this, ActCaptcha.class);
            intent.putExtra("result", status.getError());
            startActivityForResult(intent, CAPTCHA_RESULT);
            return;
        }
        else {
            if (status.getCode()==200) {
                onLoginResult(result);
            }
            else {
                ActNewsOld.alert(ActLogin.this, result == null ? getString(R.string.lgn_wrong) : result + "\n" + status.getError());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==CAPTCHA_RESULT){
            super.onActivityResult(requestCode, resultCode, data);
            if (data!=null && data.hasExtra(ActCaptcha.EXTRA_CAPTCHA_PARAMS)) {
                AQuery aq = new AQuery(activity);

                String codeParams = "";
                Bundle params = data.getExtras().getParcelable(ActCaptcha.EXTRA_CAPTCHA_PARAMS);
                for (String key : params.keySet()) {
                    String value = params.get(key).toString();
                    if (!codeParams.equals("")) codeParams+="&";
                    codeParams+=key+"="+value;
                }
                AQUtility.debug("codeParams",codeParams);
                aq.ajax("https://api.vk.com/oauth/token?grant_type=password&client_id=2965041&client_secret=fXK28HAI0nVRK3hNZiGs&username="+
                        Uri.encode(phoneTxt.getText().toString().trim())+"&password="+Uri.encode(passTxt.getText().toString())+"&scope=friends,messages,notifications,offline,nohttps,photos,audio,video,docs,notify,notes,status,groups,wall" +
                        "&"+codeParams,
                        String.class, -1, ActLogin.this, "onLogin");
            }
            else {
                ActNewsOld.alert(ActLogin.this, "No captcha data(");
            }
        }
    }

    private boolean checkFields() {
    	if (phoneTxt.getText().toString().equals("")) {
    		Animation shake = AnimationUtils.loadAnimation(ActLogin.this, R.anim.shake);
    		phoneTxt.startAnimation(shake);
    		return false;
    	}
    	if (passTxt.getText().toString().equals("")) {
    		Animation shake = AnimationUtils.loadAnimation(ActLogin.this, R.anim.shake);
    		passTxt.startAnimation(shake);
    		return false;
    	}
    	return true;
    }
    
    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActLogin.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return i;
    }

	private void onLoginResult(String result) {
		try
        {
    		JSONObject mJSONObject = new JSONObject(result);
    		AndroidApplication.putPrefsString("access_token", mJSONObject.getString("access_token"));
    		AndroidApplication.putPrefsString("secret", mJSONObject.getString("secret"));
    		AndroidApplication.putPrefsInt("user_id", mJSONObject.optInt("user_id"));
    		String code = "var user = API.users.get({uids:"+mJSONObject.optInt("user_id")+",fields:\"photo\"});" +
            		"return {user: user};";
    		restRequest(code,1);
        }
    	catch (Exception e)
        {
    		Toast.makeText(this,
					   e.getMessage(),
					   Toast.LENGTH_LONG).show();
        }
	}
	
	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (hasFocus) {
			Log.d(TAG, ""+v.getId());
			switch(v.getId())
		    {
			
		    	case R.id.inputloginr:
		    		phone.setImageResource(R.drawable.phone_active);
		    		pass.setImageResource(R.drawable.pass);
		    		break;
		    	case R.id.inputloginl:
		    		phone.setImageResource(R.drawable.phone);
		    		pass.setImageResource(R.drawable.pass_active);
		    		break;
		    }
		}
		else {
			phone.setImageResource(R.drawable.phone);
    		pass.setImageResource(R.drawable.pass);
		}
	}
}