package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.util.Comparator;

public class CmpProfiles implements Comparator<ClsProfile> {
    @Override
    public int compare(ClsProfile o1, ClsProfile o2) {
        return (int) (o2.getUid() - o1.getUid());
    }
}	