package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;

public class ClsMenuItems {
	private String itemName;
	private int itemIcon;

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemIcon(int itemIcon) {
		this.itemIcon = itemIcon;
	}

	public int getItemIcon() {
		return itemIcon;
	}
}
