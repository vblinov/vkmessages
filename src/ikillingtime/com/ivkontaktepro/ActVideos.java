package ikillingtime.com.ivkontaktepro;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 09.06.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public class ActVideos extends ActAbstractGroup {

    @Override
    public void initAdapter() {
        dlgs = new ArrayList<ClsVideos>();
        adapter = new AdpVideos(getActivity(),dlgs,getCurrentMode());
        my_string = getString(R.string.vid_my);
        customSpinner = 0;
    }

    @Override
    public void listListeners() {
        Resources r = getResources();
        final float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 150, r.getDisplayMetrics());
        //list.setColumnWidth((int)px);
        list.setOnScrollListener(onScroll);
        list.setOnItemClickListener(onListClick);
        playerControls.setVisibility(View.GONE);
        dialogs.setBackgroundDrawable(r.getDrawable(R.drawable.lightbackground));
    }

    private AbsListView.OnScrollListener onScroll = new AbsListView.OnScrollListener() {

        @Override
        public void onScrollStateChanged(AbsListView view,
                                         int scrollState) {
            //searchAsyncImageViews(view, scrollState == AbsListView.OnScrollListener.SCROLL_STATE_FLING);
        }
        /*
        private void searchAsyncImageViews(ViewGroup viewGroup, boolean pause) {
            final int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                AsyncImageView image = (AsyncImageView) viewGroup.getChildAt(i).findViewById(R.id.user_thumb);
                if (image != null) {
                    image.setPaused(pause);
                }
            }
        }
        */

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount &&
                     !isRunning && offset<dlgsSize) {

                clearList = false;
                isRunning = true;
                ((ActAbstractGroup) ActVideos.this).increaseOffset();
                onBrowse();
            }
        }
    };
    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> av, View arg1,
                                int position, long arg3) {
            if (dlgs==null || dlgs.size()<=position)
                return;

            try {
                ClsVideos video = ((ArrayList<ClsVideos>)dlgs).get(position);
                String vid = video.getVid();
                if (!viewved.contains(vid)) {
                    viewved +=vid+" ";

                    AndroidApplication.putPrefsString("v_" + getGid(), viewved);
                }
                if (video.getHref().contains("youtube"))
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse(video.getHref())));
                else {
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.parse(video.getHref()), "video/*");
                    startActivity(intent);
                }
            }
            catch (Exception e) {
                Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();

            };
        }
    };

    @Override
    public void getAlbums() {
        Bundle params = new Bundle();
        params.putString("count", ""+COUNT_MEDIUM);
        if (getGid()!=null && !getGid().equals("")) {
            params.putString("gid", ""+getGid());
        }
        request(params,1, "video.getAlbums");
    }

    @Override
    public void getSdCartMusic() {

    }

    @Override
    public void parseResult(String result) {
        if (result==null) return;
        if (clearList) dlgs.clear();
        JSONObject mJSONObject = null;
        ClsVideos film;

        try
        {

            mJSONObject = new JSONObject(result.toString());
            //mJSONObject = mJSONObject.optJSONObject("response");
            JSONArray mDialogs = mJSONObject.optJSONArray("response");

            dlgsSize = mDialogs.optInt(0);
            int first = 1;
            if (dlgsSize==0) {
                dlgsSize = 300;
                first = 0;
                //return;
            }
            for (int i=first;i<mDialogs.length();i++) {
                mJSONObject = mDialogs.getJSONObject(i);
                film = new ClsVideos();
                //Log.w(TAG, "parseresult2"+mJSONObject.optString("owner_id")+"_"+mJSONObject.optString("vid")+mJSONObject.optString("id"));
                String vid = mJSONObject.optString("owner_id")+"_"+mJSONObject.optString("vid")+mJSONObject.optString("id");
                film.setVid(vid);
                film.setId(mJSONObject.optString("vid")+mJSONObject.optString("id"));
                String img = mJSONObject.optString("image_medium");
                if (img.equals("")) img =  mJSONObject.optString("thumb");
                film.setThumb(img); //160 120
                film.setTitle(mJSONObject.getString("title"));
                film.setDescription(mJSONObject.getString("description"));
                int ddd =mJSONObject.optInt("duration");
                int seconds = (int) (ddd) % 60 ;
                int minutes = (int) ((ddd / 60) % 60);
                int hours   = (int) ((ddd / (60*60)) % 24);
                film.setDuration(""+hours+":"+(minutes<10?"0"+minutes:minutes)+":"+(seconds<10?"0"+seconds:seconds));
                film.setPlayer(mJSONObject.optString("player"));
                mJSONObject = mJSONObject.getJSONObject("files");
                String url = "";
                if (AndroidApplication.getPrefsInt("max_quality",0)==1){
                    url = Uri.decode(mJSONObject.optString("mp4_720"));
                    film.setVidtype("mp4_720");
                }
                else {
                    url = Uri.decode(mJSONObject.optString("mp4_480"));
                    film.setVidtype("mp4_480");
                }

                if (url.equals("")) {
                    url = Uri.decode(mJSONObject.optString("mp4_360"));
                    film.setVidtype("mp4_360");
                }
                if (url.equals("")) {
                    url = Uri.decode(mJSONObject.optString("mp4_240"));
                    film.setVidtype("mp4_240");
                }
                if (url.equals("")) {
                    url = Uri.decode(mJSONObject.optString("external"));
                    film.setVidtype("external");
                }
                if (url.equals("")) {
                    url = Uri.decode(mJSONObject.optString("flv_320"));
                    film.setVidtype("flv_320");
                }
                if (url.equals("")) {

                    film.setVidtype("broken");
                }
                film.setViewved(false);
                if (viewved.contains(vid))
                    film.setViewved(true);
                film.setHref(url);
                dlgs.add(film);
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, e.toString());
        }
        finally {
            mJSONObject = null;
        }
        adapter.notifyDataSetChanged();
        isRunning = false;
        if (getCacheTime()==0){
            setCacheTime(AndroidApplication.cacheTime);
            if (getCacheTime()>0) {
                firstRequest();
            }
        }

    }

    @Override
    public void processCustomAlbumSpinner(AdapterView<?> adpView,int pos) {

    }


    @Override
    public void onBrowse() {

        Bundle params = new Bundle();
        AndroidApplication.putPrefsString("CURVIDGID",(getCurrentMode()==MODE_SEARCH)?"search:"+searchQuery.trim():getGid());
        switch (getCurrentMode()) {

            case MODE_GETALL:
                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                request(params, 1, "video.get");
                break;
            case MODE_SEARCH:

                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                params.putString("q", searchQuery.trim());
                params.putString("hd", "1");
                params.putString("filters", "long");
                params.putString("adult",""+((AndroidApplication.getPrefsInt("censored",1)==1)?0:1));
                params.putString("sort",""+AndroidApplication.getPrefsInt("sort_order",0));
                request(params, -2, "video.search");
                break;

            case MODE_GETALBUM_CONTENT:

                if (aid!=null && !aid.equals("")) {
                    params.putString("aid", ""+aid);

                }
                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                params.putString("gid", ""+getGid());
                params.putString("width", "130");
                request(params,1, "video.get");

                break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
