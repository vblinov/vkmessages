package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import utils.fragmenttaskloader.AbstractTaskLoader;
import utils.fragmenttaskloader.ITaskLoaderListener;
import utils.fragmenttaskloader.TaskProgressDialogFragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class TskDialogs extends AbstractTaskLoader {
	private static final String TAG = "TskDialogs";
	private String metod, mError;
	private Map<String, String> mParams;
	
	public static void execute(FragmentActivity fa,
			ITaskLoaderListener taskLoaderListener,String _metod, Map<String, String> params) {
		
		TskDialogs loader = new TskDialogs(fa, _metod, params);

		new TaskProgressDialogFragment.Builder(fa, loader, "Wait...")
				.setCancelable(true)
				.setTaskLoaderListener(taskLoaderListener)
				.show();
	}
	
	protected TskDialogs(Context context,String _metod,Map<String, String> params) {
		super(context);
		metod = _metod;
		mParams = params;
		enableHttpResponseCache(new File(context.getCacheDir(), "http"));
		disableConnectionReuseIfNecessary();
	}
	
	private void enableHttpResponseCache(File httpCacheDir) {
        try {
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            Class.forName("android.net.http.HttpResponseCache")
                .getMethod("install", File.class, long.class)
                .invoke(null, httpCacheDir, httpCacheSize);
        } catch (Exception httpResponseCacheNotAvailable) {
        }
    }
    
    private void disableConnectionReuseIfNecessary() {
    	// HTTP connection reuse which was buggy pre-froyo
        if (Build.VERSION.SDK_INT < 0*8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

	@Override
	public Object loadInBackground() {
		HttpPost request = null;
		try {
			StringBuilder urlBuilder =  new StringBuilder("http://api.vk.com");
	    	StringBuilder md5Builder =  new StringBuilder();
	    	md5Builder.append("/method/execute");
	    	md5Builder.append("?");
	    	md5Builder.append("access_token");
	    	md5Builder.append("=");
	    	md5Builder.append(AndroidApplication.getPrefsString("access_token"));
	    	urlBuilder.append(md5Builder.toString());
	    	

	    	String sig = md5(md5Builder.toString()+"&code="+metod+ AndroidApplication.getPrefsString("secret"));
	    	//Log.d(TAG,md5Builder.toString()+"&code="+metod+ AndroidApplication.getPrefsString("secret"));
	    	urlBuilder.append("&sig=");
	    	urlBuilder.append(sig);

			request = new HttpPost(urlBuilder.toString());
			Log.d(TAG,urlBuilder.toString());
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("code", metod));
			try {
				request.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
				HttpResponse response = AndroidApplication.getClient().execute(request);
				final int statusCode = response.getStatusLine().getStatusCode();
				if (statusCode != HttpStatus.SC_OK) {
					return loadFromFile(sig);
				}

				HttpEntity getResponseEntity = response.getEntity();

				if (getResponseEntity != null) {

					String s = EntityUtils.toString(getResponseEntity);
					if (AndroidApplication.getAppCache()!=null) {
						
						try {
							File f = new File(AndroidApplication.getAppCache(), sig);
							//if (f.exists()) {
							Log.d("Write", ":"+f.getAbsolutePath());
								BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(f));
								os.write(s.getBytes());
								os.close();
							//}
						}
						catch (Exception e) {}
					}
					return s;
				}

			} 
			catch (IOException e) {
				request.abort();
				e.printStackTrace();
				return loadFromFile(sig);
			}
			return null;
		}
		catch (Exception e) {
			if (null != request) request.abort();
			e.printStackTrace();
			return null;
		}

		/*final int TIMEOUT_VALUE = 10000;
		HttpURLConnection urlConnection = null;    	
    	StringBuilder builder =  new StringBuilder();
    	StringBuilder urlBuilder =  new StringBuilder("http://api.vk.com");
    	StringBuilder md5Builder =  new StringBuilder();
    	md5Builder.append("/");
    	md5Builder.append(metod);
    	md5Builder.append("&");
    	md5Builder.append("access_token");
    	md5Builder.append("=");
    	md5Builder.append(AndroidApplication.getPrefsString("access_token"));
    	urlBuilder.append(md5Builder.toString());
    	Log.d(TAG, md5Builder.toString()+ AndroidApplication.getPrefsString("secret"));
    	///method/execute?code=return%20API.messages.getDialogs(%7B%7D)%3B&access_token=1e15f1e71bc622841bc622845c1beb1cb501bc61bcf3286b93ab4ba946f1743a7c6f56005c5da8663
//Finished:{"error":{"error_code":5,"error_msg":"User authorization failed: sig param is incorrect","request_params":[{"key":"oauth","value":"1"},{"key":"method","value":"execute"},
    	//{"key":"code","value":"return API.messages.getDialogs({});"},
    	//{"key":"access_token","value":"1e15f1e71bc622841bc622845c1beb1cb501bc61bcf3286b93ab4ba946f1743"},{"key":"sig","value":"c86a36affb982be375e141931edac465"}]}}

    	String sig = md5(md5Builder.toString()+ AndroidApplication.getPrefsString("secret"));
    	Log.d(TAG,sig);
    	urlBuilder.append("&sig=");
    	urlBuilder.append(sig);
    	
	    try {
	    	
	    	URL url = new URL(urlBuilder.toString());
	    	Log.d(TAG, urlBuilder.toString());
		    urlConnection = (HttpURLConnection) url.openConnection();
		    urlConnection.setRequestMethod("GET");
		    urlConnection.setConnectTimeout(TIMEOUT_VALUE);
		    urlConnection.setReadTimeout(TIMEOUT_VALUE);

		    urlConnection.connect();
		    int responseCode = urlConnection.getResponseCode();
		    if (responseCode == HttpURLConnection.HTTP_OK) {		    	
		    	BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
		    	String inputLine;
		        while ((inputLine = in.readLine()) != null) {
		        	builder.append(inputLine);
		        	builder.append("\n");
		        	if (isCanselled()) {
			        	return null;
			   		}
		        }
		        in.close();
		    }
		    else {
		    	return null;
		    }
	    }
	    catch (Exception e) {
			mError = "Error:"+e.getMessage();
			return null;
		}
	    finally {
	    	if (urlConnection != null)
	    		urlConnection.disconnect();
	    }
		return builder.toString();
		*/
	}

	@Override
	public Bundle getArguments() {
		return null;
	}

	@Override
	public void setArguments(Bundle args) {
		
	}
	public static final String md5(final String s) {
	    try {
	        // Create MD5 Hash
	        MessageDigest digest = java.security.MessageDigest
	                .getInstance("MD5");
	        digest.update(s.getBytes());
	        byte messageDigest[] = digest.digest();

	        // Create Hex String
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < messageDigest.length; i++) {
	            String h = Integer.toHexString(0xFF & messageDigest[i]);
	            while (h.length() < 2)
	                h = "0" + h;
	            hexString.append(h);
	        }
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    }
	    return "";
	}

	
	public static String loadFromFile(String filePath) {
		if (AndroidApplication.getAppCache()==null) 
			return null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder();
		try {
			File f = new File(AndroidApplication.getAppCache(), filePath);
			reader = new BufferedReader(new FileReader(f.getAbsolutePath()));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return builder.toString();
	}
}
