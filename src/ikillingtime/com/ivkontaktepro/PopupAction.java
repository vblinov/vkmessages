package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ScrollView;

import java.util.ArrayList;

public class PopupAction extends PopupWindows implements OnDismissListener {
	private View mRootView;
	private LayoutInflater mInflater;
	private ScrollView mScroller;	
	private OnDismissListener mDismissListener;	
	private boolean mDidAction;
    private int mAnimStyle;
    private int rootWidth=0;
    //private Context context;
    private static Activity context;
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    public static final int ANIM_GROW_FROM_LEFT = 1;
	public static final int ANIM_GROW_FROM_RIGHT = 2;
	public static final int ANIM_GROW_FROM_CENTER = 3;
	public static final int ANIM_REFLECT = 4;
	public static final int ANIM_AUTO = 5;
	

    /**
     * Constructor allowing orientation override
     * 
     * @param context    Context
     * @param orientation Layout orientation, can be vartical or horizontal
     */
    public PopupAction(Activity _context,boolean subMenu) {
        super(_context);
        context = _context;
        
        mInflater 	 = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mRootView	= (ViewGroup) mInflater.inflate(R.layout.menu,null);
        mRootView.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        if (subMenu) {
        	LinearLayout searchLay = (LinearLayout) mRootView.findViewById(R.id.menu_linearLayout);
        	searchLay.setBackgroundColor(R.color.black);
        	setupSubMenuList();
        }
        else {
        	LinearLayout searchLay = (LinearLayout) mRootView.findViewById(R.id.menu_linearLayout);
        	searchLay.setBackgroundResource(R.drawable.attach_menu);
   			setupMainMenuList();	
        }
    	
		setContentView(mRootView);
        mAnimStyle 	= ANIM_AUTO;
    }

    private void setupSubMenuList() {
    	ListView menulist=(ListView) mRootView.findViewById(R.id.menu_list);
    	menulist.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    	//menulist.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        ArrayList<ClsMenuItems> menuItems = new ArrayList<ClsMenuItems>();
        ClsMenuItems menuItem = new ClsMenuItems();
        menuItem.setItemIcon(R.drawable.attach_photo);
        menuItem.setItemName(context.getResources().getString(R.string.dlg_takephoto));
        menuItems.add(menuItem);
        menuItem = new ClsMenuItems();
        menuItem.setItemIcon(R.drawable.attach_gallery);
        menuItem.setItemName(context.getResources().getString(R.string.dlg_choosephoto));
        menuItems.add(menuItem);
        
        AdpMenu mainMenuAdapter = new AdpMenu(context, menuItems);
        //TODO null pointer?
        menulist.setAdapter(mainMenuAdapter);
        menulist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> arg0, View v, int position,
    				long id) {
    			if (position==0) {
    				ActStg.ONDISMISSCODE = 1;
    				dismiss();
    			}
    			if (position==1) {
    				ActStg.ONDISMISSCODE = 2;
    				dismiss();
    			}
    		}
        });
    }
    
    private void setupMainMenuList() {
    	ListView menulist=(ListView) mRootView.findViewById(R.id.menu_list);
    	menulist.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    	//menulist.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        ArrayList<ClsMenuItems> menuItems = new ArrayList<ClsMenuItems>();
        ClsMenuItems menuItem = new ClsMenuItems();
        menuItem.setItemIcon(R.drawable.attach_photo);
        menuItem.setItemName(context.getResources().getString(R.string.dlg_takephoto));
        menuItems.add(menuItem);
        menuItem = new ClsMenuItems();
        menuItem.setItemIcon(R.drawable.attach_gallery);
        menuItem.setItemName(context.getResources().getString(R.string.dlg_choosephoto));
        menuItems.add(menuItem);
        menuItem = new ClsMenuItems();
        menuItem.setItemIcon(R.drawable.attach_location);
        menuItem.setItemName(context.getResources().getString(R.string.dlg_share));
        menuItems.add(menuItem);
        
        AdpMenu mainMenuAdapter = new AdpMenu(context, menuItems);
        //TODO null pointer?
        menulist.setAdapter(mainMenuAdapter);
        menulist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
    		@Override
    		public void onItemClick(AdapterView<?> arg0, View v, int position,
    				long id) {
    			if (position==0) {
    				ActDialog.ONDISMISSCODE = 3;
    				dismiss();
    			}
    			if (position==1) {
    				ActDialog.ONDISMISSCODE = 4;
    				dismiss();
    			}
    			if (position==2) {
    				ActDialog.ONDISMISSCODE = 5;
    				dismiss();
    			}
    		}
        });
    }

	/**
	 * Set animation style
	 * 
	 * @param mAnimStyle animation style, default is set to ANIM_AUTO
	 */
	public void setAnimStyle(int mAnimStyle) {
		this.mAnimStyle = mAnimStyle;
	}
	
	
	public class SearchTask  extends AsyncTask<String, Integer, String> {

		@Override
	    protected String doInBackground(String... params) {
			
			String jsontext;
			StringBuilder result = new StringBuilder();
			return result.toString();
	    }
		

	    protected void onPostExecute(String result) {
 
	    }
	}
	
	View.OnClickListener onLogin = new OnClickListener() {
	      public void onClick(View v) {
	    	  
	    	  dismiss();
	      }  
	};
	
	/**
	 * Show quickaction popup. Popup is automatically positioned, on top or bottom of anchor view.
	 * 
	 */
	public void show (View anchor,boolean auto) {
		
		
		int xPos, yPos, arrowPos;
		
		mDidAction 			= false;
		
		int[] location 		= new int[2];
	
		anchor.getLocationOnScreen(location);

		Rect anchorRect 	= new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] 
		                	+ anchor.getHeight());


		if (auto)
			preShow(location[1] + anchor.getHeight());
		else
			preShow(300);
		//mRootView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
		
		mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	
		int rootHeight 		= mRootView.getMeasuredHeight();
		
		if (rootWidth == 0) {
			rootWidth		= mRootView.getMeasuredWidth();
		}
		
		int screenWidth 	= mWindowManager.getDefaultDisplay().getWidth();
		int screenHeight	= mWindowManager.getDefaultDisplay().getHeight();
		
		//automatically get X coord of popup (top left)
		if ((anchorRect.left + rootWidth) > screenWidth) {
			xPos 		= anchorRect.left - (rootWidth-anchor.getWidth());			
			xPos 		= (xPos < 0) ? 0 : xPos;
			
			arrowPos 	= anchorRect.centerX()-xPos;
			
		} else {
			if (anchor.getWidth() > rootWidth) {
				xPos = anchorRect.centerX() - (rootWidth/2);
			} else {
				xPos = anchorRect.left;
			}
			
			arrowPos = anchorRect.centerX()-xPos;
		}
		
		int dyTop			= anchorRect.top;
		int dyBottom		= screenHeight - anchorRect.bottom;

		boolean onTop		= (dyTop > dyBottom) ? true : false;

		if (onTop) {
			if (rootHeight > dyTop) {
				//yPos 			= 15;
				LayoutParams l 	= mScroller.getLayoutParams();
				l.height		= dyTop - anchor.getHeight();
			} else {
				//yPos = anchorRect.top - rootHeight;
				yPos = anchorRect.bottom;
			}
		} else {
			yPos = anchorRect.bottom;
			
			if (rootHeight > dyBottom) { 
				LayoutParams l 	= mScroller.getLayoutParams();
				l.height		= dyBottom;
			}
		}
		
		//showArrow(((onTop) ? R.id.arrow_down : R.id.arrow_up), arrowPos);
		
		setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);
		//yPos = 173;
		
		xPos = screenWidth - anchor.getWidth();
		if (auto)
			yPos = anchorRect.bottom;
		else {
			yPos = anchorRect.top-150;
		}
		//yPos = screenHeight- (anchor.getHeight()*2);
		Log.d("XY", "X:"+xPos+" Y:"+yPos);
		//06-21 13:04:07.850: E/WindowManager(6201): Activity ru.recoilme.vkmessages.ActDialog has leaked window android.widget.PopupWindow$PopupViewContainer@4097fcf0 that was originally added here
		//06-21 13:05:28.185: D/XXXY(6201): X:1232 Y:300
		//06-21 13:05:28.190: D/XY(6201): X:764 Y:1152

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, 0, yPos);
	}
	
	/**
	 * Set animation style
	 * 
	 * @param screenWidth screen width
	 * @param requestedX distance from left edge
	 * @param onTop flag to indicate where the popup should be displayed. Set TRUE if displayed on top of anchor view
	 * 		  and vice versa
	 */
	private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
		int arrowPos = requestedX;// - mArrowUp.getMeasuredWidth()/2;

		switch (mAnimStyle) {
	
		case ANIM_GROW_FROM_RIGHT:
			mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Right : R.style.Animations_PopDownMenu_Right);
			break;
		}
	}
	

	
	/**
	 * Set listener for window dismissed. This listener will only be fired if the quicakction dialog is dismissed
	 * by clicking outside the dialog or clicking on sticky item.
	 */
	public void setOnDismissListener(PopupAction.OnDismissListener listener) {
		setOnDismissListener(this);
		
		mDismissListener = listener;
	}
	
	@Override
	public void onDismiss() {
		if (!mDidAction && mDismissListener != null) {
			mDismissListener.onDismiss();
		}
	}
	
	
	/**
	 * Listener for window dismiss
	 * 
	 */
	public interface OnDismissListener {
		public abstract void onDismiss();
	}
}