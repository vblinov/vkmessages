package ikillingtime.com.ivkontaktepro;

import ikillingtime.com.ivkontaktepro.R;
import java.util.Comparator;

public class CmpDialog implements Comparator<ClsDialog> {
    @Override
    public int compare(ClsDialog o1, ClsDialog o2) {
        return (int) (o1.getMid() - o2.getMid());
    }
}	