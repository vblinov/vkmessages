package ikillingtime.com.ivkontaktepro;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.*;
import android.util.Log;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.*;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SrvLongPoll extends Service {

	  ArrayList<Messenger> mClients = new ArrayList<Messenger>();
	  /** Holds last value set by a client. */
	  int mInterval = 1000;
	  final int myID = 1234;
	  public static final String TAG = "SrvLongPoll";
	  

	  private String longKey;
	  private String longServer;
	  private String longTs;
	  //keepalive server
	  private KeepAliveConnection mKeepAliveConnection;

	  static final int MSG_REGISTER_CLIENT = 1;
	  static final int MSG_UNREGISTER_CLIENT = 2;
	  static final int MSG_SET_VALUE = 3;
	  static final int MSG_SEND_VALUE = 4;
	  
	  final Messenger mMessenger = new Messenger(new IncomingHandler());
	  

	  Handler mHandler;
	  


	  class IncomingHandler extends Handler {
		  @Override
	      public void handleMessage(Message msg) {
			  switch (msg.what) {
	          	case MSG_REGISTER_CLIENT:
	          		Log.d(TAG, "new client");
	          		
	          		mClients.add(msg.replyTo);
	                break;
	            case MSG_UNREGISTER_CLIENT:
	                mClients.remove(msg.replyTo);
	                break;
	            case MSG_SET_VALUE:
	            	mInterval = msg.arg1;
	            	
	                break;
	            case MSG_SEND_VALUE:
	                for (int i=mClients.size()-1; i>=0; i--) {
                        try {
                            mClients.get(i).send(Message.obtain(null,
                                    MSG_SET_VALUE, mInterval, 0));
                        } catch (RemoteException e) {
                            // ��������� ����� ����
                            mClients.remove(i);
                        }
	                }
	                break;
	            default:
	                super.handleMessage(msg);
	         }
	      }
	  }

	  public void startListener() {
		  new Thread(getLongPollThread).start();

		  mHandler = new Handler();
	      mKeepAliveConnection = new KeepAliveConnection();
	      new Thread(refresh).start();
	  }
	  
	  @Override
	  public int onStartCommand(Intent intent, int flags, int startId) {
	      startForeground();
	      
	      return(START_NOT_STICKY);
	  }
	  private Runnable refresh = new Runnable() {
	        public void run() {
	        	Log.d(TAG, "RUN");
	        	String s = mKeepAliveConnection.sendKeepAlive();
	        	Log.d(TAG, s);
                mHandler.postDelayed(this, 1000);
	        }
	  };

	  private Runnable getLongPollThread = new Runnable() {
	        public void run() {
	        	getLongPoll();
	        }
	  };


	  private class KeepAliveConnection {
		  private final DefaultHttpClient longClient;
		  
		  public KeepAliveConnection() {
				HttpParams params = new BasicHttpParams();
				int timeout = 1000 * 60 * 60 * 24;//
				params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
		        // Turn off stale checking.  Our connections break all the time anyway,
		        // and it's not worth it to pay the penalty of checking every time.
		        HttpConnectionParams.setStaleCheckingEnabled(params, false);

		        // Default connection and socket timeout of 10 seconds.  Tweak to taste.
		        HttpConnectionParams.setConnectionTimeout(params, 0);
		        HttpConnectionParams.setSoTimeout(params, 0);
		        HttpConnectionParams.setSocketBufferSize(params, 8192);
		        // Don't handle redirects -- return them to the caller.  Our code
		        // often wants to re-POST after a redirect, which we must do ourselves.
		        HttpClientParams.setRedirecting(params, false);
		        // Set the specified user agent and register standard protocols.
		        HttpProtocolParams.setUserAgent(params, "VK Messages");
		        SchemeRegistry schemeRegistry = new SchemeRegistry();
		        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
		        ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);
		        
		        longClient = new DefaultHttpClient(manager,params);      
		  }
		  
		  public String sendKeepAlive() {
				DefaultHttpClient localClient = longClient;
				long startTime = System.currentTimeMillis();
				HttpGet request = null;
	
				request = new HttpGet("http://"+longServer+"?act=a_check&key="+longKey+"&ts="+longTs+"&wait=25&mode=2");
				try {
					HttpResponse response = localClient.execute(request);
					final int statusCode = response.getStatusLine().getStatusCode();
					if (statusCode != HttpStatus.SC_OK) {
						Log.d(TAG, "statusCode"+statusCode);
					}
	
					HttpEntity getResponseEntity = response.getEntity();
	
					if (getResponseEntity != null) {
	
						String s = EntityUtils.toString(getResponseEntity);
						Log.d(TAG, "Success:"+longTs+":"+s);
						//06-01 19:34:34.178: D/KeepAliveService(3900): Success:{"ts":1708751841,"updates":[]}
						JSONObject mJSONObject = new JSONObject(s);
				    	
				    	longTs = mJSONObject.optString("ts");
						return s;
	
					}
	
				} 
				catch (Exception e) {
					if (request!=null) request.abort();
					e.printStackTrace();
				}
				return "";
		}
	  }
	  @Override
      public IBinder onBind(Intent intent) {
		  Log.d(TAG, "Bind");
		  startListener();
	      return mMessenger.getBinder();
	  }
	  
	  private void startForeground(){
		  Intent intent = new Intent(this, SrvLongPoll.class);
		  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		  PendingIntent pendIntent = PendingIntent.getActivity(this, 0, intent, 0);
		  Notification notice = new Notification(0, "Ticker text", System.currentTimeMillis());
		  notice.setLatestEventInfo(this, "Title text", "Content text", pendIntent);
		  notice.flags |= Notification.FLAG_NO_CLEAR;
		  startForeground(myID, notice);
	  }
	  
	  @Override
	  public void onDestroy() {
		  stop();
	  }

	  private void stop() {
		stopForeground(true);
	  }
	  
	  private boolean getLongPoll()  {
		String metod = "/method/execute?access_token="+AndroidApplication.getPrefsString("access_token");
		String code = "var messages = API.messages.getLongPollServer({});" +
        		"" +
        		"return {longpoll: messages};";
        String sig = Utils.md5(metod+"&code="+code+ AndroidApplication.getPrefsString("secret"));
        
        HttpPost request = null;

		request = new HttpPost("http://api.vk.com"+metod+"&sig="+sig);
		Log.d(TAG,"getLongPoll");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("code", code));
		try {
			request.setEntity(new UrlEncodedFormEntity(nameValuePairs,"UTF-8"));
			HttpResponse response = AndroidApplication.getClient().execute(request);
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				return false;
			}

			HttpEntity getResponseEntity = response.getEntity();

			if (getResponseEntity != null) {

				String s = EntityUtils.toString(getResponseEntity);
				Log.d(TAG, "getLongPoll result:"+s);

				JSONObject mJSONObject = new JSONObject(s);
		    	mJSONObject = mJSONObject.optJSONObject("response");
		    	mJSONObject = mJSONObject.optJSONObject("longpoll");
		    	longKey = mJSONObject.optString("key");
		    	longServer = Uri.decode(mJSONObject.optString("server"));
		    	longTs = mJSONObject.optString("ts");
				return true;
			}

		} 
		catch (Exception e) {
			if (request!=null) request.abort();
			e.printStackTrace();
		}
		return false;
	  }
}