package ikillingtime.com.ivkontaktepro;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 28.05.13
 * Time: 19:20
 * To change this template use File | Settings | File Templates.
 */
public class VisibleGoneAnimation extends ScaleAnimation {

    private View mView;

    private ViewGroup.MarginLayoutParams mLayoutParams;

    private int mMarginBottomFromY, mMarginBottomToY;

    private boolean mHide = false;

    public VisibleGoneAnimation(float fromX, float toX, float fromY, float toY, int duration, View view,
                    boolean hide) {
        super(fromX, toX, fromY, toY);
        setDuration(duration);
        mView = view;
        mHide = hide;
        mLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int height = mView.getHeight();

        if (mHide) {
            mMarginBottomFromY = 0;
            mMarginBottomToY = (int) height * (-1);
        }
        else {
            mMarginBottomFromY = mLayoutParams.bottomMargin;
            mMarginBottomToY = 0;
        }
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        if (interpolatedTime < 1.0f) {
            int newMarginBottom = 0;
            if (mHide) {
                newMarginBottom = (int) (mMarginBottomToY * interpolatedTime);
            }
            else {
                mView.setVisibility(View.VISIBLE);
                newMarginBottom = (int) ((mMarginBottomFromY)-(mMarginBottomFromY * interpolatedTime));
            }
            mLayoutParams.setMargins(mLayoutParams.leftMargin, mLayoutParams.topMargin,
                    mLayoutParams.rightMargin, newMarginBottom);
            mView.getParent().requestLayout();
        } else if (mHide) {
            mView.setVisibility(View.GONE);
        }
    }

}

