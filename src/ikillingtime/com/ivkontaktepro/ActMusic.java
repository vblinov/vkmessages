package ikillingtime.com.ivkontaktepro;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.SeekBar;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.AQUtility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileFilter;
import java.util.*;


/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 10.06.13
 * Time: 10:48
 */
public class ActMusic extends ActAbstractGroup {

    private PlayerService playerService;
    private int playerStatus;
    private boolean isPaused = false;
    private Timer progressRefresher;
    private static final String TAG = "ActMusic";
    private UiRefresher uiRefresher = new UiRefresher();
    private static ArrayList<ClsMusic> allTracksDlgs = new ArrayList<ClsMusic>();
    private static Map<String, Integer> foldersMap = new HashMap<String, Integer>();
    public static int listPosition = -1;
    private ServiceConnection playerServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder service) {

            PlayerService.PlayerBinder playerBinder = (PlayerService.PlayerBinder)service;
            playerService = playerBinder.getService();
            (new Thread(uiRefresher)).start();

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //@Override
        //public void onStart() {
        //super.onStart();
        isPaused = false;
        Intent playerServiceIntent = new Intent(getActivity(), PlayerService.class);
        getActivity().getApplicationContext().bindService(playerServiceIntent, playerServiceConnection, Context.BIND_AUTO_CREATE);

        if (playerService!=null) {
            refreshButtons();
        }
    }

    @Override
    public void onPause(){
        super.onPause();
        isPaused = true;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        if (playerService != null) {
            synchronized (playerService) {
                playerService.notifyAll();
                uiRefresher.done();
            }
        }
        if (playerService != null && getActivity()!=null) {
            getActivity().getApplicationContext().unbindService(playerServiceConnection);
        }
        playerService = null;
    }

    private class UiRefresher implements Runnable {

        private boolean done = false;

        public void done() {
            done = true;
        }

        @Override
        public void run() {

            while (!done) {
                synchronized (playerService) {
                    playerStatus = playerService.getStatus();
                    refreshTrack();
                    refreshButtons();
                    try {
                        playerService.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void refreshTracklist() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (dlgs==null) return;
                long currentTrackId = playerService.getCurrentTrackId();

                int pos = -1;
                ClsMusic m = null;
                for (int i=0;i<dlgs.size();i++){
                    m = ((ArrayList<ClsMusic>) dlgs).get(i);
                    if (m.getId()==currentTrackId) {
                        pos = i;
                        break;
                    }
                }
                if (pos<adapter.getCount()) {

                    listPosition = pos;

                    adapter.notifyDataSetChanged();
                    //list.setSelection(pos);
                    list.smoothScrollToPosition(pos);
                    list.invalidate();
                    //перерисовывает только указанный view без вызова notifyDataSetChanged
                    /*
                    int start = list.getFirstVisiblePosition();
                    for(int i=start, j=list.getLastVisiblePosition();i<=j;i++) {
                        if(m!=null && m==list.getItemAtPosition(i)){
                            View view = list.getChildAt(i-start);
                            view.setActivated(true);
                            list.getAdapter().getView(i, view, list);
                            break;
                        }
                    }
                    */
                }
            }
        });
    }

    private void refreshButtons() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                switch (playerStatus) {
                    case PlayerService.PLAYING:
                        playButton.setBackgroundResource(R.drawable.base_pause_button);
                        break;
                    default:
                        playButton.setBackgroundResource(R.drawable.base_play_button);
                        break;
                }

                if (playerService.isShuffle()) {
                    shuffleButton.setBackgroundResource(R.drawable.base_shuffle_button_on);
                }
                else {
                    shuffleButton.setBackgroundResource(R.drawable.base_shuffle_button_off);
                }
            }

        });
    }

    private void refreshTrack() {
        if (playerService==null) return;
        final boolean isTaken = playerService.isTaken();
        final int progressCurr = playerService.getCurrentTrackProgress();
        final int max = playerService.getCurrentTrackDuration();
        final String durationText = PlayerService.formatTrackDuration(max);
        final String progressText = PlayerService.formatTrackDuration(playerService.getCurrentTrackProgress());
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {


                currentTrackProgressView.setText(progressText);
                currentTrackDurationView.setText(durationText);
                trackSeek.setMax(max);
                trackSeek.setProgress(progressCurr);

            }
        });
        if (progressCurr>0 && progressCurr<=2000) {
            //начал играть
            refreshTracklist();
        }
    }


    @Override
    public void initAdapter() {
        dlgs = new ArrayList<ClsMusic>();

        adapter = new AdpMusic(activity,dlgs,((ActAbstractGroup) this));
        my_string = getString(R.string.music_my);
        customSpinner = 1;
    }

    @Override
    public void listListeners() {
        Resources r = getResources();
        dialogs.setBackgroundDrawable(r.getDrawable(R.drawable.lightbackground));
        list.setOnItemClickListener(onListClick);
        list.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount > 0 && firstVisibleItem + visibleItemCount == totalItemCount &&
                         !isRunning && offset<dlgsSize) {

                    clearList = false;
                    isRunning = true;
                    ((ActAbstractGroup) ActMusic.this).increaseOffset();
                    onBrowse();
                }
            }
        });
        progressRefresher = new Timer();
        progressRefresher.schedule(new TimerTask() {

            @Override
            public void run() {
                if (playerStatus == PlayerService.PLAYING) {
                    refreshTrack();
                }
            }
        }, 0, 500);

        playButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!playerService.isTaken()) {
                    Thread t = new Thread(){

                        public void run(){
                            playerService.play();
                            refreshTracklist();
                        }
                    };
                    t.start();
                }
            }
        });

        prevButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!playerService.isTaken()) {
                    Thread t = new Thread(){

                        public void run(){
                            playerService.prevTrack();
                            refreshTracklist();
                        }
                    };
                    t.start();
                }

            }
        });
        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (!playerService.isTaken()) {
                    Thread t = new Thread(){

                        public void run(){
                            playerService.nextTrack();
                            refreshTracklist();
                        }
                    };
                    t.start();
                }
            }
        });

        shuffleButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                boolean shuffle = playerService.shuffle();
                if (shuffle) {
                    shuffleButton.setBackgroundResource(R.drawable.base_shuffle_button_on);
                }
                else {
                    shuffleButton.setBackgroundResource(R.drawable.base_shuffle_button_off);
                }
            }
        });
        trackSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar arg0, final int pos, boolean user) {
                if (getActivity()!=null) {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            currentTrackProgressView.setText(PlayerService.formatTrackDuration(pos));
                        }
                    });
                }
                if (user) {
                    if (playerService!=null && !playerService.isTaken()) {
                        Thread t = new Thread(){

                            public void run(){
                                playerService.seekTrack(pos);
                            }
                        };
                        t.start();
                    }
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }
        });
        if (playerControls.getVisibility()==View.GONE) {
            playerControls.startAnimation(new VisibleGoneAnimation(1.0f, 1.0f, 0.0f, 1.0f, 500, playerControls, false));
        }
        setRetainInstance(true);
    }

    private AdapterView.OnItemClickListener onListClick = new AdapterView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> av, View v,
                                int position, long arg3) {
            if (dlgs==null || dlgs.size()<=position)
                return;
            final int currPos = position;

            Thread t = new Thread(){

                public void run(){
                    synchronized (playerService) {
                        playerService.refreshTracklist(dlgs,currPos);
                        playerService.play(currPos);
                        refreshTracklist();
                    }
                }
            };
            t.start();

            Handler handler=new Handler();
            final Runnable r = new Runnable()
            {
                public void run()
                {
                    saveOnSd(dlgs);
                }
            };

            handler.postDelayed(r, 5000);
        }
    };

    public void saveOnSd(final ArrayList<ClsMusic> musics){

        Thread t = new Thread() {
            public void run() {
                if (isPaused)this.interrupt();
                //if my tracks
                if (getGid()!=null && getGid().equals("") && getCurrentMode()==MODE_GETALBUM_CONTENT) {
                    ConnectivityManager connManager = (ConnectivityManager) getActivity().getSystemService(getActivity().CONNECTIVITY_SERVICE);
                    NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                    if (mWifi.isConnected()) {
                        // Do whatever
                        File currentDir = new File(
                                Environment.getExternalStorageDirectory().getAbsolutePath()+"/Music/ivkontakte");
                        if (!currentDir.exists()){
                            currentDir.mkdirs();
                        }
                        ArrayList<ClsMusic> copyMusic = new ArrayList<ClsMusic>(musics);
                        for (ClsMusic m:copyMusic) {
                            final String fileName = m.getFilename();
                            if (fileName==null || fileName.equals(""))
                                break;
                            File[] fileList = currentDir.listFiles(new FileFilter() {

                                @Override
                                public boolean accept(File file) {
                                    if (file.getName().toLowerCase().equals(fileName)) {
                                        return true;
                                    }
                                    return false;
                                }
                            });
                            if (fileList==null || fileList.length==0){

                                File target = new File(currentDir, fileName);
                                AQUtility.debug(TAG,"File:" + currentDir.getAbsolutePath() + ":" + fileName);


                                AjaxCallback<File> cb = new AjaxCallback<File>();
                                cb.url(m.getUrl()).type(File.class).targetFile(target);

                                aq.sync(cb);


                                AjaxStatus status = cb.getStatus();

                                new SingleMediaScanner(activity.getApplicationContext(),target);
                            }
                            else {
                            }
                        }
                    }
                }
            }
        };
        t.start();

    }

    @Override
    public void getAlbums() {
        Bundle params = new Bundle();
        params.putString("count", ""+COUNT_MEDIUM);

        if (getGid()!=null && !getGid().equals("")) {
            params.putString("owner_id", ""+getGid());
        }
        request(params,1, "audio.getAlbums");
    }

    @Override
    public void getSdCartMusic() {

        if (allTracksDlgs!=null && allTracksDlgs.size()>0 && foldersMap!=null && foldersMap.size()>0) {
            //from memory init
            setupSpinnerAlbum((String[]) foldersMap.keySet().toArray(new String[foldersMap.size()]));
            return;
        }
        //String[] titles = new String[1];
        ArrayList titles = new ArrayList<String>();
        titles.add(getString(R.string.music_allfolders));
        //titles[0] = getString(R.string.music_allfolders);
        Map<String, Integer> map = new HashMap<String, Integer>();
        map.put(getString(R.string.music_allfolders),1000000);
        Cursor trackCursor = getActivity().managedQuery(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Audio.Media.DATA, MediaStore.Audio.Media._ID, MediaStore.Audio.Media.TRACK}, null, null, MediaStore.Audio.AudioColumns.TRACK);

        String[] proj = {MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.YEAR, MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DURATION, MediaStore.Audio.Media.DATA};
        ClsMusic music;
        Cursor trackCursorId = null;
        allTracksDlgs.clear();
        for (int n = 0; trackCursor.moveToNext(); n++) {
            String id = trackCursor.getString(1);
            //tracks
            try {
                trackCursorId = getActivity().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, proj, "_ID = "+id+" ", null, null);
                trackCursorId.moveToNext();
                int dur = 0;
                try {
                    dur = Integer.parseInt(trackCursorId.getString(4));
                }
                catch (Exception e) {
                    continue;
                }
                if (dur==0) continue;
                music = new ClsMusic();
                music.setArtist(trackCursorId.getString(0));
                music.setTitle(trackCursorId.getString(3));
                music.setDuration(dur);
                music.setDurationStr(PlayerService.formatTrackDuration(dur));
                music.setUrl(trackCursorId.getString(5));
                music.setId(Integer.parseInt(id));

                //folders
                String s = trackCursor.getString(0),folder = "";
                String[] var = s.split("/");
                if (var.length>1) {
                    folder = var[var.length-2];
                    if (!map.containsKey(folder))
                        map.put(folder,0);
                    else
                        map.put(folder,map.get(folder)+1);
                }

                music.setFolder(folder);
                allTracksDlgs.add(music);
            }
            catch (Exception e) {

            }
        }
        map = sortByValues(map);

        foldersMap = map;
        setupSpinnerAlbum((String[]) map.keySet().toArray(new String[map.size()]));
    }

    public static <K, V extends Comparable<V>> Map<K, V> sortByValues(final Map<K, V> map) {
        Comparator<K> valueComparator =  new Comparator<K>() {
            public int compare(K k1, K k2) {
                int compare = map.get(k2).compareTo(map.get(k1));
                if (compare == 0) return 1;
                else return compare;
            }
        };
        Map<K, V> sortedByValues = new TreeMap<K, V>(valueComparator);
        sortedByValues.putAll(map);
        return sortedByValues;
    }

    @Override
    public void processCustomAlbumSpinner(AdapterView<?> adpView,int pos) {

        adapter.notifyDataSetInvalidated();
        dlgs.clear();
        if (pos==0) {
            //dlgs = new ArrayList<ClsMusic>(allTracksDlgs);   //какого хуя это не работает?
            //ClsMusic m = (ClsMusic)dlgs.get(0);
            //AQUtility.debug(TAG,((ClsMusic) dlgs.get(0)).getTitle());
            //придется как придурку перебрать(
            for (ClsMusic music:allTracksDlgs) {
                dlgs.add(music);
            }
        }
        else {
            AQUtility.debug(TAG,(String) adpView.getItemAtPosition(pos));

            for (ClsMusic music:allTracksDlgs) {
                if (music.getFolder()!=null && music.getFolder().contains((String) adpView.getItemAtPosition(pos))){
                    dlgs.add(music);
                }
            }
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    public void parseResult(String result) {

        if (result==null) return;
        AQUtility.debug(TAG,"result"+  result);
        adapter.notifyDataSetInvalidated();
        if (clearList) {
            dlgs.clear();
        }
        JSONObject mJSONObject = null;
        ClsMusic music;

        try
        {
            mJSONObject = new JSONObject(result.toString());
            JSONArray mDialogs = mJSONObject.optJSONArray("response");

            dlgsSize = mDialogs.optInt(0);
            int first = 1;
            if (dlgsSize==0) {
                dlgsSize = 300;
                first = 0;
                //return;
            }
            for (int i=first;i<mDialogs.length();i++) {
                mJSONObject = mDialogs.getJSONObject(i);
                music = new ClsMusic();
                //String vid = mJSONObject.optString("owner_id")+"_"+mJSONObject.optString("aid");
                music.setAid(mJSONObject.optString("aid"));
                music.setId(mJSONObject.optInt("aid"));

                music.setOwner_id(mJSONObject.optString("owner_id"));

                music.setTitle(mJSONObject.optString("title"));
                music.setArtist(mJSONObject.optString("artist"));
                music.setArtist(mJSONObject.optString("artist"));
                int ddd =mJSONObject.optInt("duration");
                music.setDuration(ddd*1000);
                int seconds = (int) (ddd) % 60 ;
                int minutes = (int) ((ddd / 60) % 60);
                int hours   = (int) ((ddd / (60*60)) % 24);
                music.setDurationStr((hours>0?hours+":":"")+(minutes<10?"0"+minutes:minutes)+":"+(seconds<10?"0"+seconds:seconds));
                //"http://n00.h0.imobilco.ru/dl/-/Zm49JTJGYXVkaW9ib29rc19pbXBvcnRlZCUyRjUxMSUyRjY1NiUyRjM3NiUyRjQ5OSUyRlRoZV9TdHJhbmdlX0Nhc2Vfb2ZfRHIuX0pla3lsbF9hbmRfTXIuX0h5ZGUubXAzJnVuaXQ9MCZleHA9MTM3MTIwMTYyNyZoYXNoPTk3ZTdmNTQ5MGFmNzU3Y2JiYTdkOTdhYjVkZjNjMDQw/The_Strange_Case_of_Dr._Jekyll_and_Mr._Hyde.mp3"
                music.setUrl(mJSONObject.optString("url"));
                music.setLyrics_id(mJSONObject.optString("lyrics_id"));
                String filename = mJSONObject.optString("artist").toLowerCase().trim()+"_"+mJSONObject.optString("title").toLowerCase().trim();
                StringBuilder builder = new StringBuilder();
                for (char c : filename.toCharArray()) {
                    if (Character.isJavaIdentifierPart(c)) {
                        builder.append(c);
                    }
                }
                if (getGid()!=null && getGid().equals("") && getCurrentMode()==MODE_GETALBUM_CONTENT) {
                    music.setFilename(builder.toString()+".mp3");
                }
                dlgs.add(music);
                //playerService.addTrack(playerService.new Track(music));
            }
        }
        catch (JSONException e)
        {
            Log.e(TAG, e.toString());
        }
        finally {
            mJSONObject = null;
        }

        adapter.notifyDataSetChanged();

        isRunning = false;
        if (getCacheTime()==0){
            setCacheTime(AndroidApplication.cacheTime);
            if (getCacheTime()>0) {
                firstRequest();
            }
        }
    }

    @Override
    public void onBrowse() {
        Bundle params = new Bundle();
        AndroidApplication.putPrefsString("CURMUSGID",(getCurrentMode()==MODE_SEARCH)?"search:"+searchQuery.trim():getGid());

        switch (getCurrentMode()) {
            case MODE_GETALL:
                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                request(params, 1, "audio.get");
                break;
            case MODE_SEARCH:

                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                params.putString("q", searchQuery.trim());
                params.putString("sort", "2");//Вид сортировки. 2 — по популярности, 1 — по длительности аудиозаписи, 0 — по дате добавления.
                request(params, -1, "audio.search");
                break;

            case MODE_GETALBUM_CONTENT:

                if (aid!=null && !aid.equals("")) {
                    params.putString("album_id", ""+aid);

                }
                params.putString("count", ""+COUNT_MEDIUM);
                params.putString("offset", ""+offset);
                params.putString("oid", ""+getGid());
                request(params,1, "audio.get");

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AQUtility.debug("ActMus",requestCode+"|"+data);
    }
}
