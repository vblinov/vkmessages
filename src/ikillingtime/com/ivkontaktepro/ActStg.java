package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.CursorLoader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.androidquery.util.AQUtility;
import com.google.android.c2dm.C2DMessaging;
import ikillingtime.com.ivkontaktepro.PopupAction.OnDismissListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import utils.imageloader.AsyncImageView;
import utils.imageloader.MaskImageProcessor;
import utils.serviceloader.RESTService;

import java.io.File;
import java.util.SortedMap;
import java.util.TreeMap;

public class ActStg extends Fragment {
	private static final String TAG = "ActStg";
	private static final int TAKE_PHOTO = 10;
	private static final int CHOOSE_PHOTO = 11;
	private Button logout,avatarBtn,mxplayer,youplayer_btn,stg_disablead_btn;
	private AsyncImageView avatar;
	private Activity activity;
	public static int ONDISMISSCODE = 0;
	private String mCurrentPhotoPath = "";
	private ResultReceiver mReceiver;
	private ImageView img;
	private ImageView progressBar;
    private CheckBox censored;

	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
        View v = inflater.inflate(R.layout.settings_vk, container, false);
        logout = (Button) v.findViewById(R.id.stg_logout);
        avatarBtn = (Button) v.findViewById(R.id.stg_changeavatar);
        mxplayer = (Button) v.findViewById(R.id.stg_mxplayer_btn);
        youplayer_btn = (Button) v.findViewById(R.id.stg_youplayer_btn);

        final Button evil_btn = (Button) v.findViewById(R.id.stg_evil_btn);
        final Button iface_btn = (Button) v.findViewById(R.id.stg_iface_btn);
        final Button haract_btn = (Button) v.findViewById(R.id.stg_haract_btn);

        stg_disablead_btn = (Button) v.findViewById(R.id.stg_disablead_btn);
        censored = (CheckBox) v.findViewById(R.id.stg_search_cenzored);
        final TextView fio = (TextView)v.findViewById(R.id.stg_fio);
        avatar = (AsyncImageView)v.findViewById(R.id.user_thumb);
        img = (ImageView)v.findViewById(R.id.imageView1);
        progressBar = (ImageView) v.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        
        activity = getActivity();
        
        fio.setText(AndroidApplication.getPrefsString("fio"));
        avatar.setImageProcessor(new MaskImageProcessor(7));
        avatar.setUrl(AndroidApplication.getPrefsString("photo"));
        
        logout.setOnClickListener(onLogout);
        avatarBtn.setOnClickListener(onChangeAvatar);
        mxplayer.setOnClickListener(onMxPlayer);
        youplayer_btn.setOnClickListener(onYouPlayer);
        evil_btn.setOnClickListener(onEvil);
        iface_btn.setOnClickListener(onIface);
        haract_btn.setOnClickListener(onHaract);

        if (ActTabs.adView!=null && ActTabs.adView.getVisibility()==View.GONE) {
            ((LinearLayout) v.findViewById(R.id.ad_block)).setVisibility(View.GONE);
        }
        stg_disablead_btn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ActTabs.onUpgradeAppButtonClicked(view);
            }
        });

        if (AndroidApplication.getPrefsInt("censored",1)==1){
            censored.setChecked(true);
        }
        else {
            censored.setChecked(false);
        }
        censored.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                AndroidApplication.putPrefsInt("censored",(b) ? 1 : 0);
            }
        });

        final Spinner sortOrder = (Spinner) v.findViewById(R.id.stg_vid_sort_order);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(activity,
                R.array.sort_order_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        sortOrder.setAdapter(adapter);
        sortOrder.setSelection(AndroidApplication.getPrefsInt("sort_order", 0));
        sortOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AndroidApplication.putPrefsInt("sort_order",i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final Spinner shake4next = (Spinner) v.findViewById(R.id.stg_music_shake);
        ArrayAdapter<CharSequence> adapterShake = ArrayAdapter.createFromResource(activity,
                R.array.shake_array, android.R.layout.simple_spinner_item);
        adapterShake.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        shake4next.setAdapter(adapterShake);
        shake4next.setSelection(AndroidApplication.getPrefsInt("shake_value", 0));
        shake4next.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int old = AndroidApplication.getPrefsInt("shake_value", 0);
                AndroidApplication.putPrefsInt("shake_value",i);
                if (old != 0 && old!=i)
                    ActNews.alert(activity,getString(R.string.need_relaunch));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        final Spinner maxQuality = (Spinner) v.findViewById(R.id.stg_maxquality);
        ArrayAdapter<CharSequence> adapterMQ = ArrayAdapter.createFromResource(activity,
                R.array.default_quality, android.R.layout.simple_spinner_item);
        adapterMQ.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        maxQuality.setAdapter(adapterMQ);
        maxQuality.setSelection(AndroidApplication.getPrefsInt("max_quality", 0));
        maxQuality.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                AndroidApplication.putPrefsInt("max_quality",i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        mReceiver = new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    //onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                    onRESTResult(resultCode, resultData.getString("REST_RESULT"),resultData.getInt("EXTRA_MODE", 0));
                }
                else {
                    onRESTResult(resultCode, null,0);
                }
            }
            
        };
        return v;
    }
    
    View.OnClickListener onLogout = new OnClickListener() {
      public void onClick(View v) {
    	AndroidApplication.putPrefsString("access_token", "");
		AndroidApplication.putPrefsString("secret", "");
		AndroidApplication.putPrefsString("user_id", "");
		AndroidApplication.putPrefsString("photo", "");
		AndroidApplication.putPrefsString("fio", "");
		C2DMessaging.unregister(activity.getApplicationContext());
		getActivity().finish();
    	  
      }  
    };
    
    View.OnClickListener onChangeAvatar = new OnClickListener() {
        public void onClick(View v) {
        	final PopupAction quickAction = new PopupAction(activity,true);
        	quickAction.show(v,true);
        	quickAction.setOnDismissListener(onPopupDismiss);
			quickAction.setAnimStyle(PopupAction.ANIM_GROW_FROM_RIGHT);      	  
        }  
    };
    View.OnClickListener onMxPlayer = new OnClickListener() {
        public void onClick(View v) {
            final String appName = "com.mxtech.videoplayer.ad";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        }
    };
    View.OnClickListener onYouPlayer = new OnClickListener() {
        public void onClick(View v) {
            final String appName = "ru.recoilme.lasttv";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        }
    };
    View.OnClickListener onEvil = new OnClickListener() {
        public void onClick(View v) {
            final String appName = "ru.recoilme.vkpublic.g29246653";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        }
    };
    View.OnClickListener onIface = new OnClickListener() {
        public void onClick(View v) {
            final String appName = "ru.recoilme.vkpublic.g30277672";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        }
    };
    View.OnClickListener onHaract = new OnClickListener() {
        public void onClick(View v) {
            final String appName = "ru.recoilme.vkpublic.g55156099";
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+appName)));
            } catch (android.content.ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appName)));
            }
        }
    };

    private OnDismissListener onPopupDismiss = new OnDismissListener() {

    	public void onDismiss() {
    		switch (ONDISMISSCODE) {
    		case 1:
    			runCamera();
    			ONDISMISSCODE = 0;
    			break;
    		case 2:
    			Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
    			photoPickerIntent.setType("image/*");
    			startActivityForResult(photoPickerIntent, CHOOSE_PHOTO);
    			ONDISMISSCODE = 0;
    			break;
    		}
    	
    	}
    };

    @Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//if (resultCode==0) return;//TODO test samsung
		AQUtility.debug(TAG, "resultCode " + resultCode + "requestCode" + requestCode + mCurrentPhotoPath);
		if (requestCode==CHOOSE_PHOTO) {
			if(data != null){  
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = {MediaStore.Images.Media.DATA};

	            Cursor cursor = activity.getContentResolver().query(selectedImage, filePathColumn, null, null, null);
	            if (cursor!=null) {cursor.moveToFirst();

	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            mCurrentPhotoPath = cursor.getString(columnIndex);
	            cursor.close();
	            //??????? (???? ???? ???)
	            if (!Utils.compressBmp(mCurrentPhotoPath))
	            	mCurrentPhotoPath = ""; } else mCurrentPhotoPath = "";
	        }
		}
		if(requestCode==TAKE_PHOTO && !mCurrentPhotoPath.equals("")){
			if (data!=null) {
				Log.d(TAG, data.toString());
			}
			//TODO ????? ? ????????? ??????? ???? ? ?????? ??????? ????????? ????? ??? ? ? ?????? ??????? ??? ?? ?????? ?? ??????? ??
			if (!Utils.compressBmp(mCurrentPhotoPath)) {
				//????? ?? ??????????, ?? ?????? ???? ????? ?????, ????? ? ???????????. ????????? ?? ?????? ????? ????????

                    //Samsung devices have a bug with no picture result in intent
				final ContentResolver cr = activity.getContentResolver();    
	            final String[] p1 = new String[] {
	                    MediaStore.Images.ImageColumns._ID,
	                    MediaStore.Images.ImageColumns.DATE_TAKEN
	            };                   
	            Cursor c1 = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, p1, null, null, p1[1] + " DESC");     
	            if ( c1.moveToFirst() ) {
	               String uristringpic = "content://media/external/images/media/" +c1.getInt(0);
	               Uri newuri = Uri.parse(uristringpic);
	               //???????? ???????? ??????????? ??????
	               mCurrentPhotoPath =getRealPathFromURI(newuri);
	               //???????
	               if (!Utils.compressBmp(mCurrentPhotoPath))
		            	mCurrentPhotoPath = "";
	            }
	            c1.close();
			}
		}
		if(!mCurrentPhotoPath.equals("")){
			String code = "var messages = API.photos.getProfileUploadServer({});" +
	        		"return {server: messages};";
			Bundle params = new Bundle();
		    params.putString("code", code);
			restRequest(params,5,5,"");
		}
		mCurrentPhotoPath = "";
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(activity, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void runCamera() {
		try {
			File image = Utils.createImageFile();
			mCurrentPhotoPath = image.getAbsolutePath();
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	    	takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(image));
	    	startActivityForResult(takePictureIntent, TAKE_PHOTO);
	    	
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    

    private String restRequest(Bundle params,int httpVerb,int mode,String method) {
    	//isRunning = true;
    	//progressBar.setVisibility(View.VISIBLE);
    	progressBar.setVisibility(View.VISIBLE);
    	progressBar.post(new Runnable() {
    	    @Override
    	    public void run() {
    	        AnimationDrawable frameAnimation =
    	            (AnimationDrawable) progressBar.getBackground();
    	        frameAnimation.start();
    	    }
    	});
    	StringBuilder paramsString = new StringBuilder();
    	SortedMap<String, String> paramsMap = new TreeMap<String, String>();
    	for (String key : params.keySet()) {
    		paramsMap.put(key, params.get(key).toString());
    	}

    	for (String key : paramsMap.keySet()) {
            paramsString.append("&");
            paramsString.append(key);
            paramsString.append("=");
            paramsString.append(paramsMap.get(key).toString());
    	}
    	if (method.equals("")) method = "execute";
    	String methodString = "/method/"+method+"?access_token="+AndroidApplication.getPrefsString("access_token");
		String sig = Utils.md5(methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));

		Log.d(TAG, methodString+paramsString.toString()+ AndroidApplication.getPrefsString("secret"));
        Intent intent = new Intent(activity, RESTService.class);
        intent.setData(Uri.parse(AndroidApplication.HOST+methodString+"&sig="+sig));
        intent.putExtra(RESTService.EXTRA_HTTP_VERB, httpVerb);
        intent.putExtra(RESTService.EXTRA_MODE, mode);
        intent.putExtra(RESTService.EXTRA_FILE, mCurrentPhotoPath);
        intent.putExtra(RESTService.EXTRA_PARAMS, params);
        intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);
        activity.startService(intent);
        return sig;
	}
    
    public void onRESTResult(int code, String result, int mode) {
    	Log.d(TAG, "onRESTResult:"+mode+":"+result);
    	progressBar.setVisibility(View.GONE);
    	progressBar.clearAnimation();
        if (code == 200 && result != null) {
        	if (mode == 5){
        		savePhoto(result);
        	}
        	
        	if (mode == 7) {
        		JSONObject mJSONObject = null;
				try {
					mJSONObject = new JSONObject(result);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				JSONArray mJSONArray = mJSONObject.optJSONArray("response");
        		if (mJSONArray!=null && mJSONArray.length()>0) {
        			mJSONObject = mJSONArray.optJSONObject(0);
        			
        			AndroidApplication.putPrefsString("photo", Uri.decode(mJSONObject.optString("photo")));
        			avatar.setImageProcessor(new MaskImageProcessor(7));
        	        avatar.setUrl(AndroidApplication.getPrefsString("photo"));
        		}
        	}
        	if (mode == 9) {
        		Bundle params = new Bundle();
    		    params.putString("uids", ""+AndroidApplication.getPrefsInt("user_id"));
    		    params.putString("fields", "photo");
    			restRequest(params,2,7,"users.get");
        	}
        }
        else {
        	
        }
    }
    
    private void savePhoto(String result) {
    	try {
			JSONObject mJSONObject = new JSONObject(result);
			Bundle params = new Bundle();
		    params.putString("server", mJSONObject.optString("server"));
		    params.putString("photo", mJSONObject.optString("photo"));
		    params.putString("hash", mJSONObject.optString("hash"));
			restRequest(params,2,9,"photos.saveProfilePhoto");

		} catch (JSONException e) {
			e.printStackTrace();
		}
    }
}
