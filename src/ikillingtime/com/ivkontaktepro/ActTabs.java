/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.*;
import com.androidquery.util.AQUtility;
import com.flurry.android.FlurryAgent;
import com.google.ads.AdRequest;
import com.google.ads.AdView;
import ikillingtime.com.ivkontaktepro.util.IabHelper;
import ikillingtime.com.ivkontaktepro.util.IabResult;
import ikillingtime.com.ivkontaktepro.util.Inventory;
import ikillingtime.com.ivkontaktepro.util.Purchase;

import java.util.ArrayList;


public class ActTabs extends FragmentActivity {
    static TabHost mTabHost;
    ViewPager  mViewPager;
    TabsAdapter mTabsAdapter;
    int cnt = 0;
    private static Activity activity;
    public static AdView adView;
    public static IabHelper mHelper;
    static final String TAG = "ActTabs";
    static final int RC_REQUEST = 10001;
    static final boolean debug = true;
    private LinearLayout adContainer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_tabs_pager);
        activity = ActTabs.this;
        // Create the helper, passing it our context and the public key to verify signatures with

        mHelper = new IabHelper(this, getString(R.string.publish_key));

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(debug);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        AQUtility.setDebug(debug);
        AQUtility.debug(TAG,"CREATED");

        adView = (AdView) this.findViewById(R.id.ad);
        adContainer = (LinearLayout) this.findViewById(R.id.adcontainer);
        try {
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {

                // Hooray, IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                if (!debug) {
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                    adContainer.setVisibility(View.VISIBLE);
                }
                else {
                    adView.setVisibility(View.GONE);
                    adContainer.setVisibility(View.GONE);
                }
            }
        });
        }
        catch (Exception e) {}

        if (AndroidApplication.getPrefsInt("haswall")==0 && !AndroidApplication.getPrefsString("access_token").equals("")) {
            AndroidApplication.putPrefsInt("haswall", 1);
            AndroidApplication.putPrefsString("access_token", "");
            /*
            AlertDialog.Builder b = new AlertDialog.Builder(ActTabs.this);
            b.setTitle(R.string.sorry);
            b.setMessage(R.string.new_futher);
            b.setCancelable(true);
            b.setPositiveButton("Ok",new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            b.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });
            b.create().show();   */
        }
        if (AndroidApplication.getPrefsString("access_token").equals("")) {
            finish();
            startActivity(ActLogin.createIntent(ActTabs.this));
        }
        else {
            cnt = AndroidApplication.getPrefsInt("counter",1);


            if (cnt!=-1 && cnt % 10==0) {
                start();
            }
            else {
                if (cnt!=-1) AndroidApplication.putPrefsInt("counter",cnt+1);

            }
        	DisplayMetrics metrics = new DisplayMetrics();
        	getWindowManager().getDefaultDisplay().getMetrics(metrics);
        	AndroidApplication.setLow(true);
            ConnectivityManager connManager = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
            //NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo activeNetworkInfo = connManager.getActiveNetworkInfo();
            if (activeNetworkInfo==null || !activeNetworkInfo.isConnected()){
                //noconnection
                AndroidApplication.cacheTime = 0; //use cache
            }
            else {
                if (activeNetworkInfo.getType()==ConnectivityManager.TYPE_WIFI || activeNetworkInfo.getType()==ConnectivityManager.TYPE_WIMAX || activeNetworkInfo.getType()==ConnectivityManager.TYPE_ETHERNET) {
                    //good connection
                    AndroidApplication.cacheTime = 15000;//15 sec
                }
                else {
                    AndroidApplication.cacheTime = 1000 * 60 * 15;//15 min
                }
            }
            mTabHost = (TabHost)findViewById(android.R.id.tabhost);
	        mTabHost.setup();
	
	        mViewPager = (ViewPager)findViewById(R.id.pager);
           // mViewPager.setOffscreenPageLimit(3);
	        mTabsAdapter = new TabsAdapter(this, mTabHost, mViewPager);

            addTab(getString(R.string.tab_news), R.drawable.tab_news, ActNews.class);

            addTab(getString(R.string.tab_video), R.drawable.tab_video, ActVideos.class);
	        addTab(getString(R.string.tab_con), R.drawable.tab_msg, ActDialogs.class);
            addTab(getString(R.string.tab_music), R.drawable.tab_music, ActMusic.class);
            addTab(getString(R.string.tab_contacts), R.drawable.tab_contacts, ActContacts.class);
	        addTab(getString(R.string.tab_stg), R.drawable.tab_stg, ActStg.class);
	        
	        if (!AndroidApplication.getPrefsString("tab").equals("")) {
	            mTabHost.setCurrentTabByTag(AndroidApplication.getPrefsString("tab"));
	        }
        }
    }

    private void start() {

            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
            dialog.setContentView(R.layout.ratemedialog);

            dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,R.drawable.android_sh);
            String uName = AndroidApplication.getPrefsString("fio").trim();
            if (!uName.equals(""))
                uName= getString(R.string.counter_title)+", "+uName+"!";
            else
                uName= getString(R.string.counter_title)+"!";
            dialog.setTitle(uName);
            dialog.setCancelable(true);

            TextView text = (TextView) dialog.findViewById(R.id.txt);
            text.setText(R.string.counter_msg);
            dialog.setOnCancelListener(
                    new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            AndroidApplication.putPrefsInt("counter", cnt + 1);
                            dialog.dismiss();
                        }
                    }
            );


            // Add the buttons
            Button posit,neutr,negat;
            posit = (Button) dialog.findViewById(R.id.ButtonPositive);
            neutr = (Button) dialog.findViewById(R.id.ButtonNeytr);
            negat = (Button) dialog.findViewById(R.id.ButtonNegative);

            posit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // User clicked OK button
                    AndroidApplication.putPrefsInt("counter",-1);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri
                            .parse("http://play.google.com/store/apps/details?id=ikillingtime.com.ivkontaktepro"));
                    startActivityForResult(browserIntent,100);
                    dialog.dismiss();
                }
            });

            neutr.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                    AndroidApplication.putPrefsInt("counter", cnt + 1);
                    dialog.dismiss();
                }
            });

            negat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AndroidApplication.putPrefsInt("counter",-1);
                    dialog.dismiss();
                }
            });

            dialog.show();
    }

    public static Intent createIntent(Context context) {
        Intent i = new Intent(context, ActTabs.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return i;
    }

    public static void setContact() {
    	mTabHost.setCurrentTab(4);
    }

    private void addTab(String label, int drawableId, Class _class) {
    	Intent intent = new Intent(this, ActTabs.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	TabHost.TabSpec spec = mTabHost.newTabSpec(label);

    	View tabIndicator = LayoutInflater.from(this).inflate(R.layout.tab_indicator,null);
    	TextView title = (TextView) tabIndicator.findViewById(R.id.title);
    	title.setText(label);
    	ImageView icon = (ImageView) tabIndicator.findViewById(R.id.icon);
    	icon.setImageResource(drawableId);

    	spec.setIndicator(tabIndicator);
    	spec.setContent(intent);
    	mTabsAdapter.addTab(spec,_class, null);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //outState.putString("tab", mTabHost.getCurrentTabTag());
        AndroidApplication.putPrefsString("tab",mTabHost.getCurrentTabTag());
    }

    /**
     * This is a helper class that implements the management of tabs and all
     * details of connecting a ViewPager with associated TabHost.  It relies on a
     * trick.  Normally a tab host has a simple API for supplying a View or
     * Intent that each tab will show.  This is not sufficient for switching
     * between pages.  So instead we make the content part of the tab host
     * 0dp high (it is not shown) and the TabsAdapter supplies its own dummy
     * view to show as the tab content.  It listens to changes in tabs, and takes
     * care of switch to the correct paged in the ViewPager whenever the selected
     * tab changes.
     */
    public static class TabsAdapter extends FragmentPagerAdapter
            implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
        private final Context mContext;
        private final TabHost mTabHost;
        private final ViewPager mViewPager;
        private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();

        static final class TabInfo {
            private final String tag;
            private final Class<?> clss;
            private final Bundle args;

            TabInfo(String _tag, Class<?> _class, Bundle _args) {
                tag = _tag;
                clss = _class;
                args = _args;
            }
        }

        static class DummyTabFactory implements TabHost.TabContentFactory {
            private final Context mContext;

            public DummyTabFactory(Context context) {
                mContext = context;
            }

            @Override
            public View createTabContent(String tag) {
                View v = new View(mContext);
                v.setMinimumWidth(0);
                v.setMinimumHeight(0);
                return v;
            }
        }

        public TabsAdapter(FragmentActivity activity, TabHost tabHost, ViewPager pager) {
            super(activity.getSupportFragmentManager());
            mContext = activity;
            mTabHost = tabHost;
            mViewPager = pager;
            mTabHost.setOnTabChangedListener(this);
            mViewPager.setAdapter(this);
            mViewPager.setOnPageChangeListener(this);
        }

        public void addTab(TabHost.TabSpec tabSpec, Class<?> clss, Bundle args) {
            tabSpec.setContent(new DummyTabFactory(mContext));
            String tag = tabSpec.getTag();

            TabInfo info = new TabInfo(tag, clss, args);
            mTabs.add(info);
            mTabHost.addTab(tabSpec);
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Fragment getItem(int position) {
            TabInfo info = mTabs.get(position);
            return Fragment.instantiate(mContext, info.clss.getName(), info.args);
        }

        @Override
        public void onTabChanged(String tabId) {
            int position = mTabHost.getCurrentTab();
            mViewPager.setCurrentItem(position);
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            // Unfortunately when TabHost changes the current tab, it kindly
            // also takes care of putting focus on it when not in touch mode.
            // The jerk.
            // This hack tries to prevent this from pulling focus out of our
            // ViewPager.

            TabWidget widget = mTabHost.getTabWidget();
            int oldFocusability = widget.getDescendantFocusability();
            widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
            mTabHost.setCurrentTab(position);
            widget.setDescendantFocusability(oldFocusability);

        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }
    }

    protected void onDestroy(){

        super.onDestroy();

        if(isTaskRoot()){

            //clean the file cache with advance option
            long triggerSize = 5000000; //starts cleaning when cache size is larger than 5M
            long targetSize = 2000000;      //remove the least recently used files until cache size is less than 2M
            AQUtility.cleanCacheAsync(this, triggerSize, targetSize);
        }
        if (adView != null) {
            adView.removeAllViews();
            adView.destroy();
        }

        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }
    @Override
    protected void onStart()
    {
        super.onStart();
        FlurryAgent.onStartSession(this, getString(R.string.flurry));

    }
    @Override
    protected void onStop()
    {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");
            if (result.isFailure()) {

                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase premiumPurchase = inventory.getPurchase("no_ads");
            boolean mIsPremium = (premiumPurchase != null && verifyDeveloperPayload(premiumPurchase));
            Log.d(TAG, "User is " + (mIsPremium ? "PREMIUM" : "NOT PREMIUM"));
            if (mIsPremium) {

                adView.setVisibility(View.GONE);
                adContainer.setVisibility(View.GONE);
            }
            else {
                adView.setVisibility(View.VISIBLE);
                adView.loadAd(new AdRequest());
            }

        }
    };

    /** Verifies the developer payload of a purchase. */
    static boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }
    public static void onUpgradeAppButtonClicked(View arg0) {
        Log.d(TAG, "Upgrade button clicked; launching purchase flow for upgrade.");

        /* TODO: for security, generate your payload here for verification. See the comments on
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
        String payload = "";

        mHelper.launchPurchaseFlow(activity, "no_ads", RC_REQUEST,
                mPurchaseFinishedListener, payload);
    }

    // Callback for when a purchase is finished
    static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
            if (result.isFailure()) {

                return;
            }
            if (!verifyDeveloperPayload(purchase)) {

                return;
            }

            Log.d(TAG, "Purchase successful.");

            if (purchase.getSku().equals("no_ads")) {
                // bought 1/4 tank of gas. So consume it.
                Log.d(TAG, "Purchase is gas. Starting gas consumption.");
                adView.setVisibility(View.GONE);
            }

        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        AQUtility.debug(TAG,""+requestCode+"!"+requestCode);
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            AQUtility.debug(TAG, "onActivityResult handled by IABUtil.");
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
