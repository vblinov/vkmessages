package ikillingtime.com.ivkontaktepro;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 27.06.13
 * Time: 19:56
 * To change this template use File | Settings | File Templates.
 */
public class ClsUser {
    private String uid;
    private String username;
    private String avatar;

    public ClsUser(String uid,String username,String avatar){
        this.uid = uid;
        this.avatar = avatar;
        this.username = username;
    }
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
