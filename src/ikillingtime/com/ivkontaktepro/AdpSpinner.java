package ikillingtime.com.ivkontaktepro;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.androidquery.AQuery;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: recoilme
 * Date: 26.05.13
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
public class AdpSpinner extends BaseAdapter{

    private LayoutInflater inflater;
    private ArrayList<ClsAlbums> data;
    private Activity activity;
    protected AQuery listAq;

    public AdpSpinner(Activity a,ArrayList<ClsAlbums> d) {
        data = d;
        activity = a;
        if (activity==null) return;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        listAq = new AQuery(activity);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int i) {
        return data.get(i);
    }

    public long getItemId(int i) {
        return i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = ((Activity) activity).getLayoutInflater().inflate(R.layout.spinner_row, null);
        }

        AQuery aq = listAq.recycle(view);
        aq.id(R.id.icon).image(data.get(i).getOwner_id(),true,true,30, 0, null, AQuery.FADE_IN_NETWORK, 3.0f/4.0f);
        aq.id(R.id.text).text(data.get(i).getTitle());
        return view;
    }
}
