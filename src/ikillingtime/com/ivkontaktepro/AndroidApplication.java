
package ikillingtime.com.ivkontaktepro;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.*;
import utils.imageloader.ImageCache;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


public class AndroidApplication extends Application {

    /**
     * Used for receiving low memory system notification. You should definitely
     * use it in order to clear caches and not important data every time the
     * system needs memory.
     * 
     * @author Cyril Mottier

     */
    public static interface OnLowMemoryListener {
        
        /**
         * Callback to be invoked when the system needs memory.
         */
        public void onLowMemoryReceived();
    }
    
    public static final String  HOST = "http://api.vk.com";
    public static final String 	CASHE_DIR_PATH = "Android/data/ru.recoilme.vkmessages";
	//static vars
    private static AndroidApplication sInstance;
    private static File cacheDir;
	private static HttpClient client;
	private static ClsDialog dlg;
	private static ClsVideos vid;
	private static ArrayList<ClsAttPhoto> photos;
	private static int chooseContact;
	private static int chooseContactId;
	private static boolean low;
	
	//private constants
	private static final String APP_DOMAIN = ".ru.recoilme.vkmessages";
	
	//vars
	private static SharedPreferences 	settings;
	
	 private static final int CORE_POOL_SIZE = 5;

	    private static final ThreadFactory sThreadFactory = new ThreadFactory() {
	        private final AtomicInteger mCount = new AtomicInteger(1);

	        public Thread newThread(Runnable r) {
	            return new Thread(r, "GreenDroid thread #" + mCount.getAndIncrement());
	        }
	    };

	    private ExecutorService mExecutorService;
	    private ImageCache mImageCache;
	    private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;

	    /**
	     * @hide
	     */
	    public AndroidApplication() {
	        mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
	    }

	    /**
	     * Return an ExecutorService (global to the entire application) that may be
	     * used by clients when running long tasks in the background.
	     * 
	     * @return An ExecutorService to used when processing long running tasks
	     */
	    public ExecutorService getExecutor() {
	        if (mExecutorService == null) {
	            mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE, sThreadFactory);
	        }
	        return mExecutorService;
	    }

	    /**
	     * Return this application {@link ImageCache}.
	     * 
	     * @return The application {@link ImageCache}
	     */
	    public ImageCache getImageCache() {
	        if (mImageCache == null) {
	            mImageCache = new ImageCache(this);
	        }
	        return mImageCache;
	    }

	    /**
	     * Return the class of the home Activity. The home Activity is the main
	     * entrance point of your application. This is usually where the
	     * dashboard/general menu is displayed.
	     * 
	     * @return The Class of the home Activity
	     */
	    public Class<?> getHomeActivityClass() {
	        return null;
	    }

	    /**
	     * Each application may have an "application intent" which will be used when
	     * the user clicked on the application button.
	     * 
	     * @return The main application Intent (may be null if you don't want to use
	     *         the main application Intent feature)
	     */
	    public Intent getMainApplicationIntent() {
	        return null;
	    }

	    /**
	     * Add a new listener to registered {@link OnLowMemoryListener}.
	     * 
	     * @param listener The listener to unregister
	     * @see OnLowMemoryListener
	     */
	    public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
	        if (listener != null) {
	            mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(listener));
	        }
	    }

	    /**
	     * Remove a previously registered listener
	     * 
	     * @param listener The listener to unregister
	     * @see OnLowMemoryListener
	     */
	    public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
	        if (listener != null) {
	            int i = 0;
	            while (i < mLowMemoryListeners.size()) {
	                final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
	                if (l == null || l == listener) {
	                    mLowMemoryListeners.remove(i);
	                } else {
	                    i++;
	                }
	            }
	        }
	    }

	    @Override
	    public void onLowMemory() {
	        super.onLowMemory();
	        int i = 0;
	        while (i < mLowMemoryListeners.size()) {
	            final OnLowMemoryListener listener = mLowMemoryListeners.get(i).get();
	            if (listener == null) {
	                mLowMemoryListeners.remove(i);
	            } else {
	                listener.onLowMemoryReceived();
	                i++;
	            }
	        }
	    }
	
    public static AndroidApplication getInstance() {
      return sInstance;
    }

    public static int cacheTime = 10000;

    //private static TurtlePlayer instance = null;
    @Override
    public void onCreate() {
      super.onCreate();
      Log.d("APP", "Create");
      //ACRA.init(this);
      setCacheDir();
      setClient();
      settings = getSharedPreferences(APP_DOMAIN, Context.MODE_PRIVATE);
      setChooseContact(0);
      setChooseContactId(0);
      //player = new PlayerServiceConnector(this);
      sInstance = this;
      ConnectivityManager connManager = (ConnectivityManager) this.getSystemService(this.CONNECTIVITY_SERVICE);
      NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    }
    public static AndroidApplication getStaticInstance()
    {
        if(sInstance == null){
            throw new IllegalStateException("Not initialized yet");
        }
        return sInstance;
    }

    protected void initializeInstance() {
        // do all you initialization here
    	setCacheDir();
        setClient();
	    //preferences
	    settings = getSharedPreferences(APP_DOMAIN, Context.MODE_PRIVATE);
    }
    
    public static void setClient() {
		HttpParams params = new BasicHttpParams();
		int timeout = 1000 * 60 * 60 * 24 * 7;//�� ��� ������ 24|7
		params.setIntParameter(CoreConnectionPNames.SO_TIMEOUT, timeout);
        // Turn off stale checking.  Our connections break all the time anyway,
        // and it's not worth it to pay the penalty of checking every time.
        HttpConnectionParams.setStaleCheckingEnabled(params, false);

        // Default connection and socket timeout of 10 seconds.  Tweak to taste.
        HttpConnectionParams.setConnectionTimeout(params, 0);
        HttpConnectionParams.setSoTimeout(params, 0);
        HttpConnectionParams.setSocketBufferSize(params, 8192);

        // Don't handle redirects -- return them to the caller.  Our code
        // often wants to re-POST after a redirect, which we must do ourselves.
        HttpClientParams.setRedirecting(params, true);
        // Set the specified user agent and register standard protocols.
        HttpProtocolParams.setUserAgent(params, "VK Messages");
        
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

        ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);
        
        AndroidApplication.client = new DefaultHttpClient(manager,params);
	}
	
	public static HttpClient getClient() {
		return client;
	}
    
    public static void putPrefsInt(String name, int value) {
    	SharedPreferences.Editor editor = settings.edit();
        editor.putInt(name, value);
        editor.commit();
    }
    
    public static int getPrefsInt(String name) {
    	return settings.getInt(name, 0);
    }
    public static int getPrefsInt(String name,int defVal) {
        return settings.getInt(name, defVal);
    }
    
    public static void putPrefsString(String name, String value) {
    	SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, value);
        editor.commit();
    }
    
    public static String getPrefsString(String name) {
    	return settings.getString(name, "");
    }
    
    public static String getPrefsString(String name,String defaultValue) {
    	return settings.getString(name, defaultValue);
    }


	private void setCacheDir() {
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
			cacheDir=  new File(android.os.Environment.getExternalStorageDirectory(),CASHE_DIR_PATH);
			//cacheDir= this.getCacheDir();
			if(!cacheDir.exists())
	            cacheDir.mkdirs();
		}
		else {
			cacheDir = null;
		}
	}

	public static File getAppCache() {
		return cacheDir;
	}

	public static ClsDialog getDlg() {
		return dlg;
	}

	public static void setDlg(ClsDialog _dlg) {
		dlg = _dlg;
	}

	public static int getChooseContact() {
		return chooseContact;
	}

	public static void setChooseContact(int chooseContact) {
		AndroidApplication.chooseContact = chooseContact;
	}

	public static int getChooseContactId() {
		return chooseContactId;
	}

	public static void setChooseContactId(int chooseContactId) {
		AndroidApplication.chooseContactId = chooseContactId;
	}

	public static ArrayList<ClsAttPhoto> getPhotos() {
		return photos;
	}

	public static void setPhotos(ArrayList<ClsAttPhoto> photos) {
		AndroidApplication.photos = photos;
	}

	public static boolean isLow() {
		return low;
	}

	public static void setLow(boolean low) {
		AndroidApplication.low = low;
	}

	public static ClsVideos getVid() {
		return vid;
	}

	public static void setVid(ClsVideos vid) {
		AndroidApplication.vid = vid;
	}
}