package ikillingtime.com.ivkontaktepro;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Binder;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore.Audio;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.androidquery.util.AQUtility;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

public class PlayerService extends Service {

    static public final int STOPED = -1, PAUSED = 0, PLAYING = 1;
    static public final String TAG = "PlayerService";
    private MediaPlayer mediaPlayer;
    private ArrayList<Track> tracklist,tracklistOriginal;
    private int status, currentTrackPosition;
    private AtomicBoolean taken = new AtomicBoolean(false);
    private IBinder playerBinder;
    private boolean shuffle = false;
    private TelephonyManager tm;
    private NotificationManager notificationManager;
    enum RING_STATE {
        STATE_RINGING, STATE_OFFHOOK, STATE_NORMAL
    };
    private ShakeDetectActivity shakeDetectActivity;

    @Override
    public void onCreate() {
        super.onCreate();

        mediaPlayer = new MediaPlayer();
        tracklist = new ArrayList<Track>();
        currentTrackPosition = -1;
        setStatus(STOPED);
        playerBinder = new PlayerBinder();

        mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer arg0) {
                if (currentTrackPosition == tracklist.size()-1) {
                    stop();
                } else {
                    nextTrack();
                }
            }
        });
        //restoreTracklist();
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(telephone, PhoneStateListener.LISTEN_CALL_STATE);
        //shake
        //final Vibrator vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);

        if (AndroidApplication.getPrefsInt("shake_value",0)>0) {
            shakeDetectActivity = new ShakeDetectActivity(getApplicationContext());
            shakeDetectActivity.onResume();
            shakeDetectActivity.addListener(
                    new ShakeDetectActivityListener() {
                        @Override
                        public void shakeDetected() {
                            PlayerService.this.triggerShakeDetected();
                        }
                    });
        }
    }

    public void triggerShakeDetected(){
        if (status == PLAYING && AndroidApplication.getPrefsInt("shake_value",0)>0) {
            nextTrack();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (tm != null) {
            tm.listen(telephone, PhoneStateListener.LISTEN_NONE);
            tm = null;
        }
        if (shakeDetectActivity!=null) {
            shakeDetectActivity.onPause();
        }
    }

    private PhoneStateListener telephone = new PhoneStateListener() {
        boolean onhook = false;
        RING_STATE callstaet;

        public void onCallStateChanged(int state, String number) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING: {
                    callstaet = RING_STATE.STATE_RINGING;
                    if (status==PLAYING) {
                        pause();
                        onhook = true;
                        //setResumeStop(CALL_RESUME);
                    }
                }
                break;
                case TelephonyManager.CALL_STATE_OFFHOOK: {
                    if (callstaet == RING_STATE.STATE_RINGING) {
                        callstaet = RING_STATE.STATE_OFFHOOK;
                    } else {
                        callstaet = RING_STATE.STATE_NORMAL;
                        if (status==PLAYING) {
                            pause();
                            onhook = true;
                            //setResumeStop(CALL_RESUME);
                        }
                    }
                }
                break;
                case TelephonyManager.CALL_STATE_IDLE: {
                    if (onhook) {
                        onhook = false;
                        play();
                        //setResumeStart(5, CALL_RESUME);
                    }
                    callstaet = RING_STATE.STATE_NORMAL;
                }
                break;
                default: {

                }
            }
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return playerBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    public void take() {
        taken.set(true);
    }

    private void untake() {
        synchronized (this) {
            taken.set(false);
            notifyAll();
        }
    }

    public boolean isTaken() {
        return  taken.get();
    }

    public boolean shuffle() {
        if (shuffle==true){
            // shuffle now - copy list
            tracklist = new ArrayList<Track>(tracklistOriginal);

            shuffle = false;
            return false;
        }
        else {
            //not shuffle now - save list
            tracklistOriginal = new ArrayList<Track>(tracklist);
            //shuffle
            Collections.shuffle(tracklist);
            shuffle = true;
            return true;
        }
    }
    public boolean isShuffle() {
        return this.shuffle;
    }
    private void setStatus(int s) {
        status = s;
    }

    public int getStatus() {
        return status;
    }

    public ArrayList<Track> getTracklist() {
        return tracklist;
    }

    public Track getTrack(int pos) {
        return tracklist.get(pos);
    }

    public Track getCurrentTrack() {
        if (currentTrackPosition < 0) {
            return null;
        } else {
            return tracklist.get(currentTrackPosition);
        }
    }

    public int getCurrentTrackPosition() {
        return currentTrackPosition;
    }

    public void addTrack(Track track) {
        tracklist.add(track);
        untake();
    }

    public void addTrack(int id) {
        tracklist.add(new Track(id));
        untake();
    }

    public void removeTrack(int pos) {
        if (pos == currentTrackPosition) {
            stop();
        }
        if (pos < currentTrackPosition) {
            currentTrackPosition--;
        }
        tracklist.remove(pos);
        untake();
    }

    public void clearTracklist() {
        if (status > STOPED) {
            stop();
        }
        tracklist.clear();
        untake();
    }

    public void playTrack(int pos) {
        if (status > STOPED) {
            stop();
        }

        FileInputStream file = null;
        try {
            String path = tracklist.get(pos).getPath();
            if (tracklist.get(pos).getFileName()!=null && !tracklist.get(pos).getFileName().equals("")) {
                File currentDir = new File(
                        Environment.getExternalStorageDirectory().getAbsolutePath()+"/Music/ivkontakte");
                File file1 = new File(currentDir,tracklist.get(pos).getFileName());

                if (file1.exists()) {
                    path = file1.getAbsolutePath();
                }
            }
            if (path.toLowerCase().contains("http")) {
                mediaPlayer.setDataSource(path);
                take();
                final int currPos = pos;
                mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {
                        mediaPlayer.start();
                        currentTrackPosition = currPos;
                        setStatus(PLAYING);
                        untake();
                    }
                });
                mediaPlayer.prepareAsync();
            }
            else {
                file = new FileInputStream(new File(path));
                mediaPlayer.setDataSource(file.getFD());
                mediaPlayer.setOnPreparedListener(null);
                mediaPlayer.prepare();
                mediaPlayer.start();
                currentTrackPosition = pos;

                setStatus(PLAYING);
                untake();
            }
            createNotification(tracklist.get(pos));
        } catch (Exception e) {
            Log.e(TAG,e.toString());
        }
    }

    public void createNotification(Track track) {
        if (track==null) return;
        // Prepare intent which is triggered if the
        // notification is selected
        Context context = this;
        Intent intent = new Intent(context, ActTabs.class);
        intent.setAction(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        //intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // Build notification
        // Actions are just fake
        Notification noti = new NotificationCompat.Builder(context)
                .setContentTitle(track.getArtist())
                .setContentText(track.getTitle()).setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent).build();
                //.addAction(R.drawable.base_play_button, "Play", pIntent)
                //.addAction(R.drawable.base_forward_button, "Next", pIntent).build();
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        // Hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
    }

    public void play(int pos) {
        playTrack(pos);
    }

    public void play() {
        switch (status) {
        case STOPED:
            if (!tracklist.isEmpty()) {
                playTrack(0);
            }
        break;
        case PLAYING:
            mediaPlayer.pause();
            setStatus(PAUSED);
            if (notificationManager!=null) {
                notificationManager.cancelAll();
            }
            AQUtility.debug("pause");
        break;
        case PAUSED:
            mediaPlayer.start();
            setStatus(PLAYING);
            createNotification(getCurrentTrack());
        break;
        }
        untake();
    }

    public void pause() {
        mediaPlayer.pause();
        setStatus(PAUSED);
        untake();

    }

    public void stop() {
        mediaPlayer.stop();
        mediaPlayer.reset();
        currentTrackPosition = -1;
        setStatus(STOPED);
        untake();
    }

    public void nextTrack() {
        if (currentTrackPosition < tracklist.size()-1) {
            playTrack(currentTrackPosition+1);
        }
    }

    public void prevTrack() {
        if (currentTrackPosition > 0) {
            playTrack(currentTrackPosition-1);
        }
    }

    public int getCurrentTrackProgress() {
        if (status > STOPED) {
            return mediaPlayer.getCurrentPosition();
        } else {
            return 0;
        }
    }

    public int getCurrentTrackDuration() {
        if (status > STOPED) {
            return tracklist.get(currentTrackPosition).getDuration();
        } else {
            return 0;
        }
    }

    public long getCurrentTrackId() {
        if (status > STOPED) {
            return tracklist.get(currentTrackPosition).getId();
        } else {
            return 0;
        }
    }

    public void seekTrack(int p) {
        if (status > STOPED) {
            mediaPlayer.seekTo(p);
            untake();
        }
    }

    public void storeTracklist() {
        DbOpenHelper dbOpenHelper = new DbOpenHelper(getApplicationContext());
        SQLiteDatabase db = dbOpenHelper.getWritableDatabase();
        dbOpenHelper.onUpgrade(db, 1, 1);
        for (int i = 0; i < tracklist.size(); i++) {
            ContentValues c = new ContentValues();
            c.put(DbOpenHelper.KEY_POSITION, i);
            c.put(DbOpenHelper.KEY_FILE, tracklist.get(i).getPath());
            db.insert(DbOpenHelper.TABLE_NAME, null, c);
        }
        dbOpenHelper.close();
    }

    private void restoreTracklist() {
        DbOpenHelper dbOpenHelper = new DbOpenHelper(getApplicationContext());
        SQLiteDatabase db = dbOpenHelper.getReadableDatabase();
        Cursor c = db.query(DbOpenHelper.TABLE_NAME, null, null, null, null, null, null);
        tracklist.clear();
        while (c.moveToNext()) {
            tracklist.add(new Track(c.getString(1)));
        }
        dbOpenHelper.close();
    }

    public void refreshTracklist(ArrayList<ClsMusic> musicList,int _currentTrackPosition) {

        tracklist.clear();
        for (ClsMusic m:musicList) {
            tracklist.add(new Track(m));

        }
        shuffle=false;
        currentTrackPosition = _currentTrackPosition;
        tracklistOriginal = new ArrayList<Track>(tracklist);
    }

    public class PlayerBinder extends Binder {

        public PlayerService getService() {
            return PlayerService.this;
        }
    }

    public class Track {

        private String path, artist, album, year, title, genre, fileName;
        private long id;
        private int duration;

        public Track(ClsMusic m) {
            path = m.getUrl();

            artist = m.getArtist();
            album = "";
            year = "";
            fileName = m.getFilename();
            title = m.getTitle();
            duration = m.getDuration();

            id = m.getId();
        }

        public Track(String p) {
            path = p;
            String[] proj = {Audio.Media.ARTIST, Audio.Media.ALBUM, Audio.Media.YEAR, Audio.Media.TITLE, Audio.Media.DURATION, Audio.Media._ID};
            Cursor trackCursor = getContentResolver().query(Audio.Media.EXTERNAL_CONTENT_URI, proj, Audio.Media.DATA+" = '"+path+"' ", null, null);
            trackCursor.moveToNext();
            artist = trackCursor.getString(0);
            album = trackCursor.getString(1);
            year = trackCursor.getString(2);
            title = trackCursor.getString(3);
            duration = Integer.parseInt(trackCursor.getString(4));
            id = Integer.parseInt(trackCursor.getString(5));
        }

        public Track(int id) {
            String[] proj = {Audio.Media.ARTIST, Audio.Media.ALBUM, Audio.Media.YEAR, Audio.Media.TITLE, Audio.Media.DURATION, Audio.Media.DATA};
            Cursor trackCursor = getContentResolver().query(Audio.Media.EXTERNAL_CONTENT_URI, proj, "_ID = "+id+" ", null, null);
            trackCursor.moveToNext();
            artist = trackCursor.getString(0);
            album = trackCursor.getString(1);
            year = trackCursor.getString(2);
            title = trackCursor.getString(3);
            duration = Integer.parseInt(trackCursor.getString(4));
            path = trackCursor.getString(5);
        }

        public String getPath() {
            return path;
        }

        public String getArtist() {
            return artist;
        }

        public String getAlbum() {
            return album;
        }

        public String getYear() {
            return year;
        }

        public String getTitle() {
            return title;
        }

        public String getGenre() {
            return genre;
        }

        public long getId() {
            return id;
        }

        public int getDuration() {
            return duration;
        }

        public String getFileName() {
            return fileName;
        }
    }

    public static String formatTrackDuration(int d) {
        String min = Integer.toString((d / 1000) / 60);
        String sec = Integer.toString((d / 1000) % 60);
        if (sec.length() == 1) sec = "0"+sec;
        return min+":"+sec;
    }
}
